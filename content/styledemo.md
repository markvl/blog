---
title: Style demo
permalink: /styledemo/
type: page
toc: true
---

When I started working on this page, I was planning on writing a style guide.
In the end it turned out not so much as a *guide* on how to use certain elements,
but more a *demonstration* of what certain content looks like.

Over the years I've found this page useful when redesigning this site: check how
things look or if I have forgotten to style anything. But it also serves as an
example of how to create e.g. nested lists, definition lists, figures and tables
(to name a some types of content I do not use all too often).

## Sections

### Headings

First we'll have a look at the headings.

# Example H1

Usually there is just one `<h1>` element on a page and it is used for the
title of the page, as you can see at the top of this page.

## Example H2

If an article is long enough, I divide it into sections. I use the `<h2>`
element to do so. A nice example are some of the conference note posts where
each talk gets its own section.

### Example H3

Occasionally more hierarchy is necessary and this is where the `<h3>` comes
in. To use the conference notes as an example again: sometimes a talk consists
of multiple sections and I want to mark these as such.

#### Example H4

In very rare occasions even more levels are needed and that is what the `<h4>`
is for.

##### Example H5

I don't expect to reach the `<h5>` level, but there is how that would look like.

## Grouping content

### Paragraph

Because this is a weblog, most of the content will be contained in
paragraphs, using the `<p>` element.

I am using a complete font family. This means I can use e.g. **bold** to
increase the importance of text (using `<strong>`) and _italic_ to put
emphasis on other text (using the `<em>` element). Should it be necessary, I
can even combine them to _**bold italic**_. Having separate fonts means that the
result looks better than when the browser tries to fake them. In other words, I
can [say no to faux bold](https://alistapart.com/article/say-no-to-faux-bold/).

### Code blocks

As this is a technical weblog, pieces of code are often part of the
articles. They can be included in two separate ways: inline in a
paragraph and as a preformatted chunk of text.

For code that needs to be displayed in a normal sentence, as
demonstrated earlier, use the `<code>` element. For preformatted text,
use `<code>` wrapped in a `<pre>` element, like so:

```html
<pre>
  <code>
    Put your source code here...

    <!-- And another line, but this time quite a bit longer to see what happens when a line is longer than the width of the content block. -->
  </code>
</pre>
```

### Blockquote

There are a couple of ways to quote someone. In the most simple form, it's just a
`<blockquote>`:

> Someone said this.

You can also add a source URL (which won't be visible on the page, but is
present as the `cite` attribute) and an attribution, for example:

{{< blockquote url="http://dev.w3.org/html5/spec/Overview.html#the-blockquote-element" source="HTML5 specification" >}}
The blockquote element represents a section that is quoted from
another source.

Content inside a blockquote must be quoted from another source, whose
address, if it has one, may be cited in the cite attribute.
{{< /blockquote >}}

You can, if you want to, include a link to a source in the attribution:

{{< blockquote url="https://www.gutenberg.org/cache/epub/35/pg35-images.html#id00158" cite_text="Time Machine" source="H. G. Wells" >}}
Very simple was my explanation, and plausible enough—as most wrong theories are!
{{< /blockquote >}}

### Lists

There are three types of lists: ordered lists (using `<ol>`) unordered
lists (`<ul>`) and finally description lists (`<dl>`).

In ordered lists each item is created by an `<li>` element and each item
is preceded by a number:

1. Here is an example of an item.
1. This is an item with a nested list.
    1. The first sub item.
    1. The second sub item.
1. And back to the original list but this time the list item is a bit longer just
   to see whether the line is nicely indented.

Unordered lists are similar to ordered lists in the sense that they
are consist of `<li>` elements. The difference is that the items are not
numbered.

* An item in an unordered list
* This item has a nested list:
  * Item one
  * Item two
* And a last item that contains a bit more text. This mainly shows
  that if an item spans more than a single line, it is nicely
  indented.

Description lists are created a bit differently. The `<dl>` element
must contain one or more terms (`<dt>`) which are followed by one or
more definitions (`<dd>`). Note that one term may have more
definitions and multiple terms may be related to a single definition.

Single term
:   Single definition

First term
Second term
: Single definition

Single term
: First definition
: Second definition which---just as with the lists from above---is longer just
  to demonstrate that we also have a nice indentation going on here.

### Figures

For figures, such as images, which are (according to the {{< cite >}}HTML5 spec{{< /cite>}})
{{< q >}}self-contained and [are] typically referenced as a single unit from
the main flow of the document{{< /q >}} you can use the `<figure>`
element. Optionally you can add a caption, using the `<figcaption>`
element.

For example if I would talk about the Ubuntu Circle of Friends logo, I might want to
include an image of that logo.

{{< figure
    src="/images/logo-ubuntu_cof-orange-hex.png"
    alt="Ubuntu Circle of Friends logo"
    caption="The caption of this image in which I can tell this is the Ubuntu Circle of Friends logo."
>}}

### Notes

Sometimes I want to make (side)notes in my text.

{{< note >}}
Just a sidenote.

With two paragraphs.
{{< /note >}}

Specifically for the situation where I want to make it explicit that I have
updated a post, there is this note:

{{< note type="update" date="2021-08-19" >}}
I have created a "`note`" [shortcode](https://gohugo.io/content-management/shortcodes/).
{{< /note >}}

If you look at the Markdown source of this page, you can see that I've added the
`type` parameter with the value `update`. The idea is that I want this note to
stand out from the normal text, but also draw less attention than the regular
notes.

## Text-level semantics

### Anchors

Using the `<a>` tag you can create links.

First an [example of a visited link](/styledemo/) and then an [example of an
unvisited link](/unvisited-link).

### Emphasis

To stress emphasis on a certain piece of text, use the `<em>`
element.

Example: You *must* try this fancy HTML element.

### Strong

To change the importance of text, use the `<strong>` element.

Example: Do **not** use this element too much.

### Deleted text

To mark a text as having been removed, use the `<del>` element (for example on
a todo list).

Example of `<del>`:

- ~~Write style demo~~
- Update style demo

### Cite

To represent a title of a "work" (e.g. a book, film, report, etc) use
the `<cite>` element. Most often found on this site in combination
with a quote (using the `<blockquote>` or `<q>` element).

Example: To know why 42 is an important number, you must have read {{< cite >}}The Hitchhiker’s Guide to the Galaxy{{< /cite >}}.

### Phrasing content

Content quoted from another source, not being a block of code, should
use the `<q>` element. Note that the browser add the quotation
marks.

Example: Okay, I’ll tell you: 42 is {{< q >}}the Ultimate Answer to the Ultimate
Question of Life, The Universe, and Everything.{{< /q >}}

### Inline code

To display code fragments inline, use the `<code>` element, as shown
throughout this page.

### Tabular data

For tabular data, the `<table>` element is available. There are a
number of elements related to tables, for instance `<thead>`,
`<tbody>`, `<tfoot>`, `<tr>`, `<th>`, `<td>`,
`<caption>`. I’ve combined these in one example (without the `<tfoot>`):

{{< list-table caption="The caption of this table" header=true >}}

Option
Value 1 (%):n:
Value 2 (abs):n:
Foo:n:

A first item in this table
58
377.0
&gt; 234

Second item
14
91.0
4,718

Third item
21
136.5
13,457

Last item
7
45.5
56,728

Total
100
650.0
&gt; 75,137

{{< /list-table >}}

Note that the rows are separated by a horizontal line. This should make it
easier to follow a row on larger tables while being less ‘heavy’ than e.g. using
a background colour every other row.
