---
title: Open tabs — December 2022
date: 2022-12-30
tags: [book, docker, homelab, security, tabs]
toc: true
---

The end of the year is a nice time to review my open tabs on my phone and
computer to see what's worth saving and what is not. So here is
[another round](/tags/tabs/).

<!--more-->

Note that I do not necessarily endorse the articles or applications I link to.
Most of the links to tools are here specifically because they seem interesting
to me, but I have no actual experience with them---hence the need for a reminder
on this list.

I have tried to group the links somewhat, but other than that they are listed in
more or less random order.

## Development

[Autodocumenting Makefiles](https://daniel.feldroy.com/posts/autodocumenting-makefiles)
: A nice trick to document your `Makefile`.
This article was also discussed on [Hacker News](https://news.ycombinator.com/item?id=30137254).

[gron](https://github.com/TomNomNom/gron)
: From the `README`: {{< q >}}gron transforms JSON into discrete assignments to make it
easier to grep for what you want and see the absolute 'path' to it. It eases the
exploration of APIs that return large blobs of JSON but have terrible
documentation.{{< /q >}}

[asdf](https://asdf-vm.com/)
: A version manager for e.g. Ruby, Node.js, Python.

[Shell Script Best Practices](https://sharats.me/posts/shell-script-best-practices/)
: Some rules of thumb for writing shell scripts which were also
[discussed on Hacker News](https://news.ycombinator.com/item?id=33354286).

[How to change git default branch from master](https://levelup.gitconnected.com/how-to-change-git-default-branch-from-master-3933afab08f9)
: I had to (or wanted to) switch from using the name "master" for my main branch
to something else ("main" in most cases) for a couple of Git repositories. It is
not hard, but if you do not do it often, it is convenient to have a guide like this
to make sure you do not forget anything.

## Blogs

[Bitfield Consulting](https://bitfieldconsulting.com/)
: I'm linking the whole website here since it has a bunch of nice [Go related
articles](https://bitfieldconsulting.com/golang) but also interesting articles
in the [blog](https://bitfieldconsulting.com/blog).

[Kristiāns Kronis' blog](https://blog.kronis.dev/articles)
: I have a couple of articles on this blog still open to (finish) reading, like
[Using Ubuntu as the base for all of my containers](https://blog.kronis.dev/articles/using-ubuntu-as-the-base-for-all-of-my-containers),
[Moving from GitLab CI to Drone CI](https://blog.kronis.dev/tutorials/moving-from-gitlab-ci-to-drone-ci) and
[On burnout](https://blog.kronis.dev/articles/on-burnout).

[Valentine's blog](https://www.vharmers.com/)
: Informative blog of which I still want to read the last two articles in the
[OpSec blog series](https://www.vharmers.com/tags/opsec/).

[linuxserver.io blog](https://www.linuxserver.io/blog)
: A blog by the community that maintains "the largest collection of Docker
images on the web" (their words).

## Security

[The Personal Infosec & Security Checklist](https://www.goldfiglabs.com/guide/personal-infosec-security-checklist/)
: Actionable best practices to harden your security posture.

[Personal security checkist](https://security-list.js.org/#/)
: Tips for protecting your digital security and privacy.

[A Defensive Computing Checklist](https://defensivecomputingchecklist.com/)
: Another list of tips on how to make your digital life more safe.

[Router Security](https://routersecurity.org/)
: A site with the focus on the security of routers. From the same author as the
previous link.

[Aegis-icons](https://aegis-icons.github.io/)
: Unofficial set of icons for the [Aegis Authenticator](https://getaegis.app/)
application.

[Pentester's Promiscuous Notebook](https://ppn.snovvcrash.rocks/)
: Notes by and for a pentester.

## Homelab

[BaptisteBdn/docker-selfhosted-apps](https://github.com/BaptisteBdn/docker-selfhosted-apps)
: A GitHub repository with guides on how to run a bunch of applications via
Docker.

[Dockerholics Application List](https://petersem.github.io/dockerholics/)
: Another list of applications you can host yourself using Docker containers.

[Awesome-Selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)
: Yet another (_the_?) list of applications you can run yourself.

[Awesome Sysadmin](https://github.com/awesome-foss/awesome-sysadmin)
: A list of Free and Open-Source sysadmin resources.

[Watchtower](https://containrrr.dev/watchtower/)
: Automatically update your Docker containers if newer images are available.

[Diun](https://crazymax.dev/diun/)
: If you do not like the idea of automatically updating your containers with
Watchtower, you might want to look at this **D**ocker **I**mage **U**pdate
**N**otifier application.

[Drone](https://www.drone.io/)
: I'm already running a [Gitea](https://gitea.io/en-us/) instance and a
[Docker Registry](https://hub.docker.com/_/registry). Drone might be a nice
third component to automatically build projects and e.g. create Docker images
and push them to my internal registry.

[Khue's Homelab](https://homelab.khuedoan.com/)
: Khue Doan has a project to provision, operate and update his homelab. As such
it is a nice inspiration.

[Grafana Loki](https://grafana.com/oss/loki/)
: A log aggregation system that looks like a useful addition to my setup, since
I'm already running [Grafana](https://grafana.com/grafana/) to visualise some
metrics.

[Vector](https://vector.dev/)
: This also looks like a interesting tool to collect logs.

[Step Certificates](https://github.com/smallstep/certificates)
: I am already using a private certificate authority to create certificates for
the services in my homelab, but it would be nice to have a self hosted
[ACME server](https://www.rfc-editor.org/rfc/rfc8555) to do the tedious work.
This tool might be what I need.

## Entertainment

[Elevator Saga](https://play.elevatorsaga.com/)
: Fun game where you program an elevator/set of elevators to meet certain criteria.

[Movie of the Night](https://www.movieofthenight.com/)
: While officially a {{< q >}}movie/series recommendation engine{{< /q >}} I use
this site regularly to check if I can stream a movie or series in my country and
if so, on which service it is available.

[OSMC](https://osmc.tv/)
: An interesting looking open source media center, which you can run on a
Raspberry Pi or on their devices, like the [Vero 4K+](https://osmc.tv/vero/)

[The Lazarus Heist: From Hollywood to High Finance: Inside North Korea's Global Cyber War](https://www.amazon.com/Lazarus-Heist-Hollywood-Finance-Inside/dp/024155425X)
: A book about the
[Lazarus Group](https://en.wikipedia.org/wiki/Lazarus_Group), tipped in
[Darknet Diaries episode 119](https://darknetdiaries.com/transcript/119/).

[The Silver Ships Series](https://scottjucha.com/silverships.html)
: A book series by Scott Jucha, tipped in the
[Security Now podcast](https://twit.tv/shows/security-now). (The series is
mentioned in [episode 887](https://www.grc.com/sn/sn-887.htm) for the first
time.) I've finished the first book and loved reading it!

[Open Circuits](https://nostarch.com/open-circuits)
: A lovely book about electronic components with beautiful pictures.

## Miscellaneous

[Schrijf een persoonlijk manifest voor richting in je werk en leven](https://pfauth.com/intentioneel-leven/persoonlijk-manifest/) (Dutch)
: I'm not sure I'll ever write such a personal manifest, but just reading this
article gave me enough food for thought to make some decisions.

[How to Use File History in Windows 10](https://www.lifewire.com/use-file-history-in-windows-10-3891070)
: Useful article for people that want to backup and restore files on
Windows machines.

[Restic](https://restic.net/)
: I'm in the process of testing this backup tool to see if I want to switch over
to restic from my current `rsync` based script to backup my Linux machines to an
external disk. If I go that route, I'll also have to have a look at
[restic-tools](https://github.com/binarybucks/restic-tools).

[Creating Fully Reproducible, PDF/A Compliant Documents in LaTeX](https://shen.hong.io/reproducible-pdfa-compliant-latex/)
: When I was perparing my CV and a cover letter, I wanted the resulting PDF to
be more accessible. This article gave me useful instructions on how to achieve
that.

[Using Neopixels with the Raspberry Pi](https://thepihut.com/blogs/raspberry-pi-tutorials/using-neopixels-with-the-raspberry-pi)
: I'm toying with the idea of upgrading my home office with some LED
strips. Perhaps I'll use Neopixels and a Raspberry Pi (or similar board) to do
this.

[Things your manager might not know](https://jvns.ca/blog/things-your-manager-might-not-know/)
: An article about how you can help your manager (help you).
