---
title: Pulling Docker images via a SOCKS5 proxy
date: 2022-05-10
tags: [docker, proxy, ssh]
---

This post describes how you can work around a firewall to pull Docker images
from a server that you do not have direct access to, using a SOCKS5 proxy.

<!--more-->

## Why would you want to do this?

Let us assume you are in a corporate environment. And let us further assume that
you want to pull Docker images from a registry that you do not have direct
access to from that corporate environment, for instance because the registry is
running on a non-standard port and is thus blocked by a firewall.

![The direct connection between the laptop and server is not allowed](/images/blocked_connection.svg)

So for instance the following does not work for you:

```bash
docker pull registry.example.com:5678/image
```

Now let's make a few more assumptions:

- You have got SSH access to a machine outside of the corporate environment.
- You are not violating any policy by bypassing the firewall, or have permission
  to do so.

## Workaround

To work around the issue, you can do the following:

- Connect to a machine over SSH
- Tunnel your `docker pull` command via that SSH connection.

![Using a proxy to tunnel your traffic](/images/allowed_connection.svg)

### Setting up the SOCKS5 proxy connection

You are going to use the `-D` option in your SSH command. This allocates a
socket listing on a port. Connections made to this port are forwarded over the
(secure) channel.

For example, if you can use host `172.31.10.5` as a proxy, the command would look
like this:

```plain
ssh -D 8080 172.31.10.5
```

Every connection to port `8080` on `localhost` is proxied via host `172.31.10.5`.

### Configure Docker

To make Docker use the proxy, you will have to configure `dockerd`. One way to do
this is to create the file `/etc/systemd/system/docker.service.d/proxy.conf`
with the following content:

```ini
[Service]
Environment="HTTP_PROXY=socks5://127.0.0.1:8080"
Environment="HTTPS_PROXY=socks5://127.0.0.1:8080"
```

(You most likely do not even need the `HTTP_PROXY` line, but it also doesn't
hurt. ;-) )

Once this file is in place, you need to restart the Docker service:

```bash
systemctl daemon-reload
systemctl restart docker
```

When you run the following command again, the traffic is tunneled via your proxy.

```bash
docker pull registry.example.com:5678/image
```

Voilà, the firewall is bypassed and you can now pull your Docker image.
