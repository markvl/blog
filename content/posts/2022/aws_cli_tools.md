---
title: AWS CLI tools I've used
date: 2022-12-07
tags: [aws, cli, mfa, sso, tools]
---

A quick post (mostly for myself) to list the command line tools I've used over
the years to interact with AWS, besides the official AWS Command Line Interface
(AWS CLI).

<!--more-->

I was content with the AWS CLI initially, but once I had enabled and required
multi-factor authentication (MFA), using the AWS CLI became a bit of a nuisance.
So I started using **[aws-mfa](https://github.com/broamski/aws-mfa)**. I also
wrote a post about it here, titled [Using MFA with AWS
CLI](/2019/02/24/using-mfa-with-aws-cli/).

The next tool I started using was **[Awsume](https://awsu.me/)**. This Python
package makes it easier to manage your AWS credentials and sessions, especially
if you are accessing more than one account.

Once we started using Single Sign-On (SSO) more and more at work, I switched to
using **[AWS Vault](https://github.com/99designs/aws-vault)**. I _think_ the
reason I switched to AWS Vault was simply because Awsume did not support SSO,
but to be honest, I am not sure about that. (Neither do I know if Awsume
supports SSO at the moment.) Either way, what I really like about AWS Vault is
that it stores the credentials in an encrypted password store. So I no longer
had credentials lying around on my hard drive in plain text.

After reading the article
[Taking AWS Account Logins For Granted](https://www.lastweekinaws.com/blog/taking-aws-account-logins-for-granted/),
I also switched to using **[Granted](https://granted.dev/)**. In contrast to AWS
Vault, Granted does not seem to have a way to securely store your long lived
credentials. If you are using those, you may want to use AWS Vault for those.
Since both tools use the same `~/.aws/config` file, you can use them next to each
other without a problem.

And that's the current state of affairs for me. Since I effectively only deal
with SSO logins, I use Granted and its `assume` command for my day-to-day.
Combined with the
[Granted add-on for Firefox](https://addons.mozilla.org/en-GB/firefox/addon/granted/)
I can easily use multiple accounts and roles on the command line _and_ in my
browser.
