---
title: Removing the comments from this website
date: 2020-02-11
tags: [blog]
---

I've taken the step to removed the comments from my website. In this
article I'll give some stats and offer a bit of an explanation.

<!--more-->

Let's start with some fun facts.

The first comment was placed on the article
[IOError: [Errno 28] No space left on device](/2009/05/27/ioerror-errno-28-no-space-left-on-device/).
Over the last 10 years a total of 457 comment have been placed. (At least, that is
the number according to [Disqus](https://disqus.com/). I may have thrown away
some spam in the past.) Of these, only 210 are actually published---all the
others are spam.

A total of 45 articles got commented on. The top three most commented articles:

- [Locked myself out of root account on EC2 Ubuntu instance](/2010/09/06/locked-myself-out-root-account-ec2-ubuntu-instance/) (26 comments)
- [Merge a separate Git repository into an existing one](/2013/11/02/merge-a-separate-git-repository-into-an-existing-one/) (24 comments)
- [IntegrityError: duplicate key value violates unique constraint](/2011/05/06/integrityerror-duplicate-key-value-violates-unique-constraint/) (17 comments)

To each and everyone that placed a comment here: **thank you!**

So why remove the comments then? The main reason is that the option to comment
here adds bloat. Just to show the Disqus form your browser has to make 17
requests and download about 286 KB of data.

_To be fair: if I write an article which uses all the fonts and their variants I
define on this site, about 220Kb of data is transferred. So that is also an area
where I can improve. But that is something for another day._

So from now on, I you want to send me feedback on an article, you will have to
[contact me](/about/#contact) via e.g. [Twitter](https://twitter.com/mvlent) or
[email](mailto:mark@vlent.nl).
