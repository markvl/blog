---
title: Devopsdays oNLine 2020
date: 2020-07-09
tags: [conference, devops]
toc: true
---

This year, due to the [COVID-19 pandemic](https://en.wikipedia.org/wiki/COVID-19_pandemic),
the Dutch devopsdays are combined into a single day, online event.

<!--more-->

![devopsdays oNLine logo](/images/devopsdays2020_logo.jpg)

These are my notes from the keynote sessions.

## Why change matters now more than ever: the importance of DevOps in this new world --- Michael Ducy

This year has challenged us in many ways. Many people got sick, many people have
died. Our world has been turned upside-down. Some responses to this have been
good, some bad. We're more divided, communities have been shattered.

DevOps and digital transformation came about due to the rise of cloud and
mobile. The relationship between businesses and consumers has changed. Businesses
interact with us differently than before. DevOps is a response to serve digital
transformation.

DevOps is about community and empathy. Digital transformation is really
about capitalism, about finding new ways to exploit capital and resources. This
is an interesting contrast since DevOps is to support digital transformation.

Are the digital possibilities we have nowadays, which have helped us in these
challenging times (Netflix, order food online, etc), contributing for the good?
Or do they divide the people that are fortunate enough to be able to use them
and those that are not? What does digital transformation do to serve the
underprivileged, the poor, the oppressed?

Be very aware that there are _many_ people that are being oppressed. Many
systems in our world are built upon the "technical debt" of not treating people
as equals. We intentionally do not go into the bad neighbourhoods in our cities.
We avoid them (at least in part) to not be confronted with the conditions that
people from those parts of the city have to live in. We don't want to be
reminded about what we are _not_ doing to help these people out, while enjoying
our own lives.

In the DevOps community we talk about diversity and inclusion. We have the
option to close a number of gaps. It is not just about the difference in pay
between genders. It is also about us, privileged people, to use our voices to
lift people up. We must invest in lifting up the lowest and destroy our systems
of oppression that were setup hundreds of years ago.

> Think about how you can use your voice to destroy systems of oppression.

There is a lot that is built into society and culture that needs to be changed.
We have an opportunity to lead the change.

## From waterfall to agile. Microsoft's not-so-easy evolution into the world of DevOps --- Abel Wang

The definition of DevOps used by Microsoft:

{{< blockquote source="Donovan Brown" >}}
DevOps is the union of people, process and products to enable continuous
delivery of value to our end users.
{{< /blockquote >}}

The first lesson Microsoft had to learn in their evolution from shipping a boxed
software product to a service: if it hurts, don't avoid it but do it more often.
By doing it more often, you get better at it over time until eventually it stops
hurting.

The way you organize your company is very important. You don't want different
teams competing with each other, you want them to collaborate, to be one
company.

To support that idea Microsoft started to use one engineering system instead of
allowing each team to pick their own tools. Now Microsoft is using their own
tools (Azure DevOps services: Azure Boards, Azure Pipelines, etc).

Microsoft's definition of done:

- Live in production
- Collecting telemetry

To get there, dramatic changes in the team structure was needed. Previously
there were basically three teams: program management, development and testing.
There was a toxic culture between the development and the QA teams. QA was seen
as a second class citizen.

They transitioned to an engineering team where the development and QA teams
became one team: QA had to learn the codebase and the developers needed to learn
how to test. Another transition they made was to create a "feature team," which
combined engineering, ops and program management. This makes it easier for
customers to communicate with Microsoft since one team is responsible for one
feature, instead of different teams, depending on where in the development phase
a feature was at a certain time.

Engineers are allowed to pick a different team to work in each year. This makes
them very happy even though only about 20% of the developers actually changes to
a different team.

Another change is that every team has the same sprint cadence now. All sprints
start on the same day and have the same duration (3 weeks). Sprints start with
an email to the engineers and end with a summary of what has been accomplished
plus a short video. At the end of every 4 sprints, all team leads (of the teams
that are related to each other) have a planning meeting. Every 6 months the 12
month strategy is reviewed.

Leadership wanted to micromanage, but didn't have enough context. Providing them
the right context to make sound decisions took too much time. In the end it was
decided that leadership was no longer allowed to look at the backlog and only
operate at the 6-month and 12-month planning level.

The teams also introduced a new rule: if you find a bug, you have to fix it in
that sprint. Teams are only allowed to carry four bugs to the new sprint. This
is called the "bug cap." If you are over this number, you are not allowed to
build new features the next sprint but first have to reduce the number of open
bugs. This is not meant as a punishment, but as a way to keep quality at a more
stable level.

What to measure? Be very careful because people tend to want to game the system.
Is code coverage important for your bonus? Be prepared to have 100% code
coverage without actually improving the quality of the code. It is better to
measure outcome, e.g. how much time it takes to fix a bug once found. Metrics
are **not** used to punish or reward engineers---they are used to _help_ the
engineers and answer questions like "do you need training on the new framework?"

How do you get your developers to do the right thing? Make them feel the pain of
their own code if they don't. Make them part of the call to solve a live site
incident for example.

## Securing your DevOps transformation --- April Edwards

(For the record: April also works for Microsoft and uses the same definition for
DevOps as Abel.)

Why is DevOps important? Because your competition is also using it. It results
in high performance companies that deploy more often, have a lower change
failure rate and respond to the market faster (shorter time between commit and
deployment).

{{< blockquote source="Gartner Inc." >}}
95% of cloud breaches occur due to human errors such as configuration mistakes
{{< /blockquote >}}

We are constantly sacrificing security for speed. Things that hinder security:

- Manual processes that can be automated
- Friction between teams
- A "we'll do it later" approach

Ops and dev teams don't always have the same goals. We need to align that and
own a feature together.

_(Due to a technical issue the talk could not be completed as planned and April
had only a few moments to wrap up.)_

The main takeaway: "shift left" because most defects are introduced during the code
writing phase. Address these things earlier and tackle security during
development. Add security checks to your pipelines.

## BizDevOps: bridging the dominant divide between business and IT --- Henk van der Schuur

There shouldn't be a divide between the business and IT. Both sides should cross
over to the other side. IT needs someone from business, and business should take
more ownership over tech decisions and understand what they mean. Both sides
should be part of one team. It should be a partnership.

You can start a project with a "pressure cooker session" to get a good start. Such
a session consists of a number of phases:

- Prepare: what is this (really) about?
- Ideate: do we completely understand the problem?
- Sketch: is this what you mean and what should be changed?
- Prototype: what are the most important requirements and who is the audience?
- Decide: was value delivered? what's next?

If this session ends with a "go" all disciplines should be involved: business,
operations and development.

![The interaction between business, operations, development and product owner.](/images/devopsdays2020_henk_van_der_schuur.jpg)

DevOps needs a sequel. Let's repeat the trick and involve the business. Get
the business, operations, developers and product owners into the same room. The
business wants to get involved and wants us to realise that it's about
delivering value. Technology is everyone's business.
