---
title: Replacing the TLS certificate on a Synology NAS via the command line
date: 2020-10-02
tags: [cli, synology, tls]
---

Yesterday was the day that the TLS certificate of my Synology NAS expired. And
since I have no monitoring to alert me, I only found out today. The bad news:
[HSTS](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
was also enabled so my browser did not want to connect, even though I told it to
ignore the invalid certificate. The good news: the SSH service was enabled. This
allowed me to fix this situation via the command line interface (CLI).

<!--more-->

Before I dive into what I did to resolve the situation:

- I am running DSM version 6.2.3 on a DS216+.
- There might be better ways to resolve this, but I could not find any within a
  reasonable time.

The process of requesting or generating a new certificate is out of scope for
this article. And if you are in the same situation as I was (with an expired
certificate installed on the device), the "get a new certificate" part is
probably something you have dealt with before. In my case I generated my own
certificate signed by my own CA.

After obtaining the new certificate I could:

- Use `scp` to copy the new certificate and the private key to the NAS.
- SSH into the Synology.
- Change my directory to `/usr/syno/etc/certificate/system`.
- Update the relevant files, both in the `FQDN` and `default` directory, with
  the new certificate and key (details below).
- Reboot the NAS.

Now for the details about the files. Both the `default` directory and the `FQDN`
directory contained the same files:

- `cert.pem`: the certificate itself (in my case the certificate + intermediary)
- `chain.pem`: the certificate chain (in my case the CA certificate)
- `fullchain.pem`: a concatenation of the files `cert.pem` and `chain.pem`
- `privkey.pem`: the private key

Since the script I use to generate my certificates outputs the certificate plus
intermediate, I guess that's what I used in the web interface when I uploaded my
certificate a year ago. I decided to do the same this time since it had been
working so far.

After the reboot the web interface was using the new certificate and I could
access the NAS again with my browser. But according to the control panel, I was
still using an expired certificate.

![The Synology NAS control panel still shows an expired
certificate](/images/synology_control_panel_certificate.png)

So as a last step I updated the certificate via the web interface and after that
everything was working again.
