---
title: Soldering 101
date: 2020-06-10
tags: [hardware]
---

Yesterday evening I did something I had not done before: I soldered a bit.

<!--more-->

I had gotten a soldering iron a few months ago, but never got around to using
it. To be honest, I was a bit hesitant to get started—I was afraid to destroy
one of my Arduino boards or sensors.

But a couple of weeks ago I came across the "HHTronik Quarantine Occupation
Kits" on Kickstarter. That was exactly what I needed and I ordered both the
through-hole and surface mount device (SMD) beginner sets. I also watched the
(Dutch) [Soldering 101](https://www.youtube.com/watch?v=RAdFQD9x1lQ) video from
Sebastian Oort to learn a bit about soldering.

So yesterday I took the plunge and got started. Since I'm an absolute beginner,
I started with the the through-hole kit for my first "project." HHTronik
provides excellent, detailed
[instructions](https://github.com/hhtronik/qok-i-learn-soldering-th) which made
my life a whole lot easier.

![The end result of my first soldering experiment](/images/soldering_qok_th.jpg)

As you might be able to see, I can still use a bit of practice. But I'm happy
with the result, the light actually works and most importantly: I enjoyed
myself! It was not as hard as I expected and I will probably try the SMD kit
soon.

If you feel inspired, I can highly recommend one of the
[Q.O.K I learn soldering](https://hhtronik.com/product/q-o-k-i-learn-soldering)
kits from HHTronik.
