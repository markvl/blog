---
title: Ubuntu Jaunty Jackalope
date: 2009-05-30T18:14:00
tags: [apple, python, ubuntu]
---

My experiences with upgrading Ubuntu 8.10 (Intrepid Ibex) to Ubuntu
9.04.

<!--more-->

This time I decided to not
[wait for four months](/2009/02/19/upgrade-ubuntu-8.04-to-8.10/)
before updating to the new Ubuntu version. Especially since colleagues
reported to work successfully with this new version. I've worked with
this setup for a week or two now and everything seems to work. Here's
what I encountered:

## Python

I expected to run into problems with Python since the Ubuntu packages
for e.g. Python imaging and XML (which I need for my day-to-day work)
aren't available for version 2.4 anymore. While
[Maurits van Rees](https://maurits.vanrees.org/weblog/archive/2009/03/using-ubuntu-9-04-beta)
and [Sam Stainsby](https://web.archive.org/web/20090502235022/sam.stainsby.id.au/blog/?p=28) chose to work
with virtualenv, I installed the required packages globally.

[Setuptools](https://pypi.org/project/setuptools/) and
PIL were relatively easy to
install. Just a matter of downloading and running "`/usr/bin/python2.4
setup.py install`" (IIRC, to be honest I didn't document this.)
However, the `libxml2` python bindings proved to be a bit harder. I
needed the following steps to get it up and running (thanks to
[Hedley Roos and Izak Burger](https://web.archive.org/web/20090427050925/http://www.upfrontsystems.co.za/Members/hedley/my-random-musings/compile-and-install-libxml2-python)):

- Download version 2.6.32 since that is the version of the Ubuntu
  `limxml2` package.
- Unpack the tarball, run "`./configure`" and "`make`"
- Go to the Python directory and execute:
  ```bash
  $ CFLAGS=-I/usr/include/libxml2 /usr/bin/python2.4 setup.py build
  $ sudo CFLAGS=-I/usr/include/libxml2 /usr/bin/python2.4 setup.py install
  ```

## Loud beep on shutdown

The new version of Ubuntu also brought a loud surprise: every time I
powered down my laptop a beep was audible. Since it seemed to bypass
the sound driver (muting the sound didn't help) I could not even turn
it down. Changing the Gnome settings
[as recommended](https://ubuntuforums.org/showthread.php?p=7215341) did
not work; my Dell Precision M65 kept on making the noise.

Luckily the solution provided on
[another thread](https://ubuntuforums.org/showthread.php?t=1151784) did
work. I added the following line to `/etc/modpobe.d/blacklist.conf`:

```plaintext
blacklist pcspkr
```

And I enjoy the silence once again. As an additional benefit: no more
lound beeps if I make mistakes (e.g. trying to scroll pas the end of
my Tomboy note, try to use backspace at the beginning of a line in
XChat, et cetera). So in a way I'm grateful of the loud beeps at
shutdown: I wasn't annoyed enough by the other beeps to silence them
but I'm glad I got rid of them. And so do the people in my
environment. :)

## Apple keyboard

At [Zest](https://zestsoftware.nl/) we use the thin (wired) Apple keyboards.
They look slick and work quite well. The problem I had with them (besides that
we've got the Dutch keyboard layout, which has the tilde placed near the "z"
instead of where if should be: next to the "1" key) is that the function keys
didn't work. That is: I had to push the "fn" key. Otherwise pressing F12 turned
up the volume instead of bringing up Firebug.

On Intrepid I solved this already by having a file called
`/etc/modprobe.d/applekeyboard` with the following contents:

```plaintext
options hid pb_fnmode=2
```

Apparently this didn't work anymore. Brian K. White on the other hand
presented a solution that worked for me.

I've now got `/etc/modprobe.d/hid_apple.conf` with this line:

```plaintext
options hid_apple fnmode=2
```

After updating initramfs (“``update-initramfs -k `uname -r` -u``”) and rebooting
the machine, the function keys worked the way I like them. (While writing this
blog entry and checking my sources, I see that this information is now also
available on the
[AppleKeyboard](https://help.ubuntu.com/community/AppleKeyboard#Ubuntu%209.04%20(Jaunty%20Jakalope))
page in the Ubuntu Community Documentation.)
