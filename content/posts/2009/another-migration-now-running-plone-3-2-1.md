---
title: "Another migration: now running Plone 3.2.1"
slug: another-migration-now-running-plone-3.2.1
date: 2009-02-19T23:32:00
tags: [blog, plone]
---

A quick fix became an upgrade. :)

<!--more-->

Since I started to blog, I wanted to register it on Planet Plone. What
held me back was the fact that the RSS feed showed my Plone username
as the creator and not my full name. I never got around to fix
this. Since I had some time at hand anyway, I decided to set this
straight. And while I'm in
[an upgrading mood](/2009/02/19/upgrade-ubuntu-8.04-to-8.10 "Upgrade
Ubuntu 8.04 to 8.10"), why not also upgrade the site to Plone 3.2.1?

As expected it went smoothly. But hey, that's one of the advantages of
having a simple site (almost no customizations, little content). ;-)
I'll also try to get my blog listed on Planet Plone soon now I've got
the RSS sorted out.
