---
title: "IOError: [Errno 28] No space left on device"
slug: ioerror-errno-28-no-space-left-on-device
date: 2009-05-27T20:58:00
tags: [plone]
---

For the second time in the one month, I ran into this problem. Here's
how I solved it. (As a reminder for myself the next time I need it...)

<!--more-->

During a migration of the Plone site of one of our customers, I ran
out of space on `/tmp`. Frustrating, especially if you've been waiting
for quite some time already. If you've ran into this problem or expect
that you will not have enough space in the tmp directory, start the
instance like this:

```plaintext
$ TMPDIR=/path/to/alternative/tmp/dir bin/instance start
```

Now the temporary stuff will be written to
`/path/to/alternative/tmp/dir` instead of `/tmp`.

{{< note type="update" date="2009-05-30" >}}
The rest of this post was added after a comment by Wouter.
{{< /note >}}

I tried to include the alternative `tmp` directory in the buildout
configuration: I'm under the impression that just adding the lines in the
`[instance]` section doesn't work directly: the `<buildout>/var/tmp` directory
doesn't seem to be generated when running "`bin/buildout`" and thus Zope doesn't
use it. I solved this by using the
[ore.recipe.fs](https://pypi.org/project/ore.recipe.fs/) package:

```ini
[parts]
    ...
    tmp

[tmp]
recipe = ore.recipe.fs:mkdir
path = ${buildout:directory}/var/tmp

[instance]
...
environment-vars =
    TMP ${tmp:path}
```

If anyone knows a better way (Wouter?) please let me know...
