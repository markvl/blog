---
title: Airport and airline conspiracy
date: 2009-06-30T12:46:00
tags: [opinion]
---

After spending most of the afternoon on an airport it finally hit me:
there's more than meets the eye to the whole process of boarding a
plane.

<!--more-->

Yesterday in the early afternoon the travel agency picked us up at our
hotel and brought us to
[Corfu International Airport](https://en.wikipedia.org/wiki/Corfu_International_Airport). And
there it all began:

- Standing in line to check-in.
- Waiting.
- Standing in line for the security check.
- Waiting at the gate.
- Standing in line to board the plane.

Somewhere in the couple of hours we were at the airport it occurred to
me that there may be more reasons for this process than just
logistics. By having people stand in lines and waiting for some time,
they will become somewhat numb. Which is a good thing if you want to
cram as many people as possible in a limited space for a couple of
hours...

Perhaps the above is quite obvious for frequent flyers, it was a small
revelation to me. Oh well, I'm glad to be home again. :)

_PS Our holiday was great actually. That's not the reason I'm happy to
be home._
