---
title: Migrating to Hugo
date: 2021-12-10
tags: [blog, hugo]
toc: true
---

Once again have I rebuilt this website with a completely different technology
stack. This post dives into some of the details.

<!--more-->

_To be honest I've written this post mostly for myself. I've edited all my posts
(see below) and it was fun to read about the (major) changes I made to this site
over the years and rediscover what I did and why I did it._

_So here's another chapter..._

![Hugo logo](/images/hugo-logo.svg)

## Bye bye Acrylamid, hello Hugo!

I last changed the technology stack that generates this website in 2012 when I
[migrated to Acrylamid](/2012/10/01/migrating-to-acrylamid/). I've been happy
with that choice for years but Acrylamid has been unmaintained since 2016 and I
wanted to use something else.

I shopped around for static site generators that allow me to write my posts in
Markdown. After looking at a couple of options I decided to pick
[Hugo](https://gohugo.io/). I like it because:

- It's open source ([GitHub repo](https://github.com/gohugoio/hugo))
- Hugo is actively developed on
- It seems to be quite popular (currently number 2 of the generators listed
  [Jamstack.org](https://jamstack.org/generators/), based on the number of stars
  on GitHub)
- There is [extensive documentation](https://gohugo.io/documentation/)
- It offers the flexibility I wanted
- There are quite some [themes](https://themes.gohugo.io/) available (at the moment
  there are 285 listed with the tag "blog" for example)

## Process

At first I started out with one of the themes
([Noteworthy](https://github.com/kimcc/hugo-theme-noteworthy)) to get
up-and-running quickly. Later on in the process I started building my own theme,
using bits and pieces from Noteworthy. Funny thing is that I ended up with
something looking very similar to what I had for my Acrylamid site.

I chose to not render any raw HTML code in my Markdown. To compensate I have
created a couple of [custom Hugo shortcodes](https://gohugo.io/templates/shortcode-templates/)
to be able to e.g. use the `<blockquote>` element with a citation attribute or
create tables. On the one hand it removes some flexibility you have when you can
write raw HTML, on the other hand it forces me to do things more consistently by
using the shortcodes.

Having to replace the raw HTML plus fix lots of broken links, led to the decision
to review each and every blog post I have written here. There are only about 150
of them, so that didn't seem like a massive job... In the end it took me way
more time than I expected to go through them all.

_It was also fun to read the old posts, like the ones I wrote during
[my first Plone conference](/tags/ploneconf2008/) in 2008. And when I read the
[open tabs](/2018/05/12/open-tabs/) post I made in 2018 I rediscovered a book I wanted
to read. And although I still haven't actually read [Dark
Matter](https://www.goodreads.com/book/show/27833670-dark-matter) yet, I have
purchased it and uploaded it to my e-reader._

While working on this version of my website I took the advice given by Rain Leander in her
[2021 devopsdays Amsterdam talk](/2021/06/29/devopsdays-online-2021/#getting-started-embrace-your-inner-child-----rain-leander):
{{< q >}}Playing is instrumental to life. No one can tell you how to play. Do
what is fun for you. And when it stops being fun, just stop doing it.{{< /q >}}
As a result I alternated between editing old posts, working on the theme,
picking fonts, etc. Or not work on it at all if I didn't feel like it. This
meant I could have finished this sooner, but that's okay---it's only a hobby
project.

## Theme

As I've said: I started out with an existing theme, but ended up with creating
my own theme. Unfortunately I do not have a clean separation between content and
theme at the moment. I might want to improve this in the future, but it is the
reason why I haven't made a separate Git repository for the theme now: the
project and theme are too tightly coupled.

The way this new site looks is only slightly different from
what it looked like before. Under the hood I'm using newer CSS layout options:
[grids](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Grids) and
[flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox).
I cannot call myself an expert on these topics, but it sure was fun to play
around with them.

I also wanted to jump on the [variable
font](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Fonts/Variable_Fonts_Guide)
train. I liked the idea of only having to include a single font which can then
be used in all kind of different weights. However I was not able to
get it down to an acceptable size. The closest I got was still about 5 times the
size of a regular font, which means a much bigger chunk of data to download for
the visitors of this site.

As a result I'm sticking to standard fonts for now. I picked the fonts I liked
on [Google Fonts](https://fonts.google.com/) and used
[google-webfonts-helper](https://google-webfonts-helper.herokuapp.com/fonts) to
download the files needed to self-host them. My choice at this moment:

- [Merriweather](https://fonts.google.com/specimen/Merriweather) for the main content
- [Fira Sans Extra Condensed](https://fonts.google.com/specimen/Fira+Sans+Extra+Condensed)
  for the headings and other texts that need to stand out (menu, post meta data, footer, etc)
- [Fira Code](https://fonts.google.com/specimen/Fira+Code) for code samples

## Build times

Although I never had complaints about the speed of Acrylamid in the past, Hugo
is quite a bit faster. This became obvious when I was writing my
[notes for All Day DevsOps](/2021/10/28/all-day-devops-2021/). I had been five
months since I had written something here, but in the meanwhile I had been
working with Hugo to edit my old posts and create the theme. And I had gotten
used to [LiveReload](https://gohugo.io/getting-started/usage/#livereload)
which in practice meant that a page had already reloaded in my web browser in
the time it took to switch from my editor to my browser.

The live reload alone makes the process feel much snappier, but I was curious
about the actual difference in build times between Acrylamid and Hugo. So I
decided to test the two use cases that are most important for me since this is
what I do most of the time:

- A complete rebuild, e.g. to deploy to the live environment (without artifacts from
  experiments/drafts, etc)
- When I create/edit a post I have Acrylamid/Hugo watch the files and rebuild
  the site on change.

I ran these scenarios three times and took the shortest run for both tools.

{{< list-table header=true >}}

Action
Acrylamid (ms):n:
Hugo (ms):n:

Complete rebuild
1497
397

Watch
1300
95

{{< /list-table >}}

This confirms what I had already suspected: Hugo takes only a fraction of the
time Acrylamid needs when rebuilding the site after I change a post (the 2nd use
case). Combined with the live reload this makes for a **big** difference in my
workflow.

## New domain

For this version of this site I also switched to a new domain. I got the
`vlent.nl` domain in 2003 and used it for my website ever since. Now, 18 years
later, I've decided to start using a different domain for this site:
`markvanlent.dev`.

Although this frees up the `vlent.nl` domain for other stuff in the future, I
have setup a redirect for (I think) all the URLs I was
using. At first I wasn't sure if it would be worth the effort, but after going
through 13 years of blog posts I can tell you that it is really nice if old URLs
still exist or at least redirect to a new location for the content.

## New hosting

I've been managing my own virtual private servers for this site since
[2013](/2013/05/15/new-home-for-this-site/) and it was fun. It offered me a lot
of control: [deployment](/2019/04/10/new-blog-backend/#code-repository),
[web server](/2019/04/10/new-blog-backend/#serving-the-content),
[TLS configuration](/2014/04/19/ocsp-stapling-in-nginx/), et cetera---I was all up to
me. And that's also the downside: there was only 1 person maintaining it: me. It
doesn't cost me that much time, but it's something I have to take care of
nonetheless. And if e.g. a new version of Docker
[no longer proxies IPv6 requests by default](https://github.com/moby/moby/issues/41858),
it's also up to me to (a) detect the update broke something and (b) solve it.

Because maintaining a VPS no longer brings me joy, and I've heard good things
about [Netlify](https://www.netlify.com/), I thought this was a good moment to
try them out. Especially now that I'm using Hugo, one of the [common build
configurations](https://docs.netlify.com/configure-builds/common-configurations/),
switching to Netlify quite simple.

## Future

I might want to tweak the theme in the future or see if I can have a more clear
separation between the theme and the content. But for the moment I'm happy and
enjoying the fact that this project is finished.
