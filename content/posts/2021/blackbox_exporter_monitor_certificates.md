---
title: Monitoring TLS certificate expiry
date: 2021-06-23
tags: [grafana, prometheus, tls, tools]
---

This is a short follow-up article to the [NAS TLS certificate
replacement](/2020/10/02/replacing-the-tls-certificate-on-a-synology-nas-via-the-command-line/)
one I wrote a few months back. Since then I have set up monitoring of the TLS
certificates I've deployed.

<!--more-->

My initial idea was to build some custom tool which would fetch the
certificates, inspect them and output the expiry date (or the numbers of days
left until that date). And although that would have been fun, I also didn't get
around to it for some time. Meanwhile more certificates were about to expire...

I already had [Grafana](https://grafana.com/) running for another dashboard and
I have some experience with [Prometheus](https://prometheus.io/). So using the
[Blackbox exporter](https://github.com/prometheus/blackbox_exporter) to solve
the data collection part was---in hindsight---an obvious solution for my
problem. Importing a community built
[dashboard](https://grafana.com/grafana/dashboards/13659) in Grafana was the
next logical step.

![Screenshot of my Grafana dashboard displaying Blackbox exporter data](/images/blackbox_exporter_grafana.png)

This gives me just about everything I need and a bit more. I peek at this
dashboard every once in a while and thus far have been able to replace
certificates _before_ they expire.
