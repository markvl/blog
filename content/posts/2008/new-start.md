---
title: New start...
slug: new-start..
date: 2008-08-24T02:36:00
tags: [blog, ]
---

I've finally taken the time to setup the new vlent.nl website. Plone 3
combined with some add-on products and a bit of customization are the
driving forces behind the current incarnation of this site.

<!--more-->

After my PHP site stopped working some time ago, I had the vague plans
to start using Plone to setup my new site. My marriage of August 20,
2008 gave that plan a new boost. After all: a website is an easy way
to distribute the photo's taken that day.

## Technology

The backbone of this site is formed by Plone 3 (3.1.4 at the time of
writing, to be exact). To spice things up a bit, I used the add-one
products [Scrawl](https://pypi.org/project/Products.Scrawl/) (for this blog)
and [Slideshow Folder](https://pypi.org/project/Products.slideshowfolder/) (to
display the wedding photo's nicely).

The changes to the look-and-feel of the site are mainly done via
CSS. Although I have to admit that I did also change some templates
and views to 'get it right'.

## Hosting

My employer, [Zest Software](https://zestsoftware.nl), allowed me to
host this site on one of their servers. So, in a way, I'm eating my
own dog food. ;-)
