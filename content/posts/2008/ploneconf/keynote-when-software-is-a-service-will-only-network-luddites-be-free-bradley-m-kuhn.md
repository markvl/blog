---
title: "Keynote: When Software Is a Service, Will Only Network Luddites Be Free? (Bradley M. Kuhn)"
slug: keynote-when-software-is-a-service-will-only-network-luddites-be-free-bradley-m.-kuhn
date: 2008-10-08T21:26:00
tags: [plone, ploneconf, ploneconf2008]
---

Bradley encourages us to critically think about how free Gmail and
Twitter actually are.

<!--more-->

Bradley is a programmer who started diving into licenses. He works
with the Software Freedom Law Center now.

In the beginning there were no licenses, just the Four Freedoms:

- to learn,
- to copy and share,
- to modify and
- to share modified versions.

To make a long story shore: someone convinced hardware manufacturers
to sell licenses for their software. Fortunately, there is lots of
software right now you can install and that give the user the four
freedoms back again.

But what about software you use through your browser: Software as a
Service (SaaS)? Gmail for instance is free, as in price, but does it
give you freedom? We need to step back and take a look at the policy
for software. The challenge is to regain the power to move your
applications, data and human connections. We need to be able to get
our data and relocate our community.

Interesting projects in this area:

- Evan Prodromou's [identi.ca](https://identi.ca/)
- Jesse Vincent's [Prophet](https://syncwith.us/)

Interesting licence options:

- [Affero GPL](https://en.wikipedia.org/wiki/Affero_General_Public_License)
- Extend copyleft to network service world

For more info see [autonomo.us](https://web.archive.org/web/20081212175033/http://autonomo.us/).
