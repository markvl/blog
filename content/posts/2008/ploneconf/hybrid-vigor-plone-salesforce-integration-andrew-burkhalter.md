---
title: Hybrid Vigor Plone / Salesforce integration (Andrew Burkhalter)
slug: hybrid-vigor-plone-salesforce-integration-andrew-burkhalter
author: Jean-Paul Ladage
date: 2008-10-09T22:40:00
tags: [plone, ploneconf, ploneconf2008]
---

Constituent Relationship Management

<!--more-->

**(Contribution by Jean-Paul Ladage)**

Salesforce is:

- a web based CRM
- an online accessible relational database

Goals for integration:

- Login again Salesforce records and update profile data (PAS)
- Simple event registration
- View Salesforce data

He provided a few demo's.

## Sale data from PloneFormGen to Salesforce using Salesforce

PloneFormGen adapter. This allows you to create forms and directly
submit the data to salesforce.

- Account adapter, you can map the form fields to fields in the account table in
  the Salesforce database.
- Contact adapter, you can map the form fields to fields in the contact table in
  the Salesforce database.
- Attachments can also be added.

## RSVP for Salesforce

Event registration e.g. call for proposals

CRM has a campaign feature. Any piece of content in Plone can be
associated with a campaign in sales force by selecting the object type
and uid. A result is that in Plone below the content a contact form
is shown. It adds a lead in Salesforce and shows how we got in contact with
that person (by showing the related campaign.

In Salesforce you can create a waitlist state, to signal that you have to
process the contact.

## Login and edit user profile data (PASPlugin)

- You can add a password field to Salesforce.
- In the plugin you can configure which fields to authenticate with
  and map additional properties like department, or assistant name
  with an object type in Salesforce e.g. a Contact or Lead.

## Local Cache of Salesforce records

The Zope catalog is used to cache information from Salesforce. Using an
overnight scheduled job.
