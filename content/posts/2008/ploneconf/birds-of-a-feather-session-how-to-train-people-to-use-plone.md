---
title: "Birds of a Feather Session: How to Train People to Use Plone"
slug: birds-of-a-feather-session-how-to-train-people-to-use-plone
date: 2008-10-11T02:31:00
tags: [plone, ploneconf, ploneconf2008]
---

Brief summary of the discussed issues.

<!--more-->

In this session we discussed issues involved in training people to use
Plone. While the developers and integrators may love the way Plone can
be changed, extended, etc. it makes the job harder for the ones that
have to train the end users: the training cannot be a generic. Each
client has its own customizations, additional content types, etc. that
need to be discussed.

Having said that, there is generic material available. In random order
(and certainly not complete):

- ~~`www.plonebook.info`~~ (does not exist any longer)
- [Documentation on plone.org](https://docs.plone.org/)
- ~~`plone.tv`~~ (does not exist any longer)
- ~~`learnplone.org`~~ (also no longer exists)

Several forms of teaching users have been mentioned:

-   training sessions,
-   written documentation,
-   screencasts,
-   use Selenium to illustrate actions as an addition to written documentation.

Users can also be guided to do the right thing by using content rules,
as [Joel already discussed](/2008/10/08/real-world-intranets-joel-burton/
"Real world intranets (Joel Burton)"). This could either reduce the
need for training al together, or assist people to remember what
taught during the training.

The best thing would be having an interface that works so intuitively
that no training whatsoever is needed. However, that is another
subject...
