---
title: Controlling Chaos (Mike Robinson)
slug: controlling-chaos-mike-robinson
date: 2008-10-09T23:40:00
tags: [plone, ploneconf, ploneconf2008]
---

A talk about agile development.

<!--more-->

Agile development is a framework more suitable to software
development. The principles of agile could even be used in the
waterfall model. Even traditional manufacturers (e.g. in the car
industry) are switching over to Agile.

Agile values:

- Individuals and interactions (over processes and tools)
- Working software (over comprehensive documentation)
- Customer collaboration (over contract negotiation)
- Responding to change (over following a plan)

(Note that the values in the parentheses are still valuable, just less
than the preceding value.)

The [Agile principles](https://agilemanifesto.org/principles.html)
(abbreviated):

- Customer satisfaction has the highest priority.
- Welcome changing requirements.
- Deliver working software frequently.
- Developers must work together daily.
- Build projects around motivated individuals.
- Give them the environment to get the job done.
- Working software is the primary measure of progress.
- Face-to-face communication.
- Sustainable development.
- Maintain a constant pace indefinitely.
- Technical excellence and good design.
- Simplicity.
- Self-organizing teams.
- The team reflect the effectivity and change its behaviour
  accordingly.

Notes:

- You should releases regularly, even if those releases won't be put
  on the market.
- The written requirements should not contain too much detail. Each
  requirement is a placeholder for contacting the client at the time
  the requirement gets implemented; only then the details will be
  discussed in depth.
- Showing a customer an unfinished product (to check whether you are
  on the right track) can throw off the customer. Sometimes they
  cannot see through the rough interface. This can be solved by
  showing a prototype. If the prototype is good, the code behind it
  can be filled.

Agile is about building the right thing and building it the right way.

The stories you are writing should describe features that deliver
value for the customer. Adding e.g. a database does not interest the
customer. Either explain the customer that the first of the features
X, Y and Z will take 4 times longer to get this setup. Or, as
fallback, include some infrastructure setup story. But:

- keep this to a minimum and
- deliver at least one feature in the first iteration.

After discussing aspects of agile development, Mike split us into
teams. By conducting iterations (estimate features, let the client
prioritize them, agree on a set of features to implement, implement
them, review with the customer) we got to create monsters from
Play-Doh. A nice way to introduce people unfamiliar with agile
development to the concept.

In closing, I can recommend the book "The Art of Agile Development" by
James Shore and Shane Warden if you are looking for a good read about
a practical approach to agile development.
