---
title: "Keynote: The state of Plone (Alexander Limi, Alan Runyan)"
slug: keynote-the-state-of-plone-alexander-limi-alan-runyan
date: 2008-10-08T19:14:00
tags: [plone, ploneconf, ploneconf2008]
---

Things to be proud of, work to be done and the way ahead.

<!--more-->

Alexander and Alan started their talk with quite a number of things
they and the community can be proud of. Amongst others:

- Lots of documentation, including an end user manual.
- The upgrade of plone.org, which is not just an upgrade to the latest version,
  but also includes the usage of ZODB Blobs. The goal is to make plone.org the
  Plone equivalent of the [cheeseshop](https://wiki.python.org/moin/CheeseShop).
  (There is even a new design for the plone.org front page.)
- There is seems to be a consolidation of products. Instead of just
  adding new ones, products are also removed and merged; the best
  solutions are coming together.

There are also areas that need attention:

- The end user manual needs to be translated to more languages.
- The documentation needs to be kept up-to-date (e.g. removing dead
  links).
- Plone can be promoted even more. The
  [World Plone Day](https://plone.org/events/wpd/wpd) (November 7th,
  2008) would be a nice opportunity. :)

The roadmap of the 3.x series of Plone is at the moment quite
dull. That is, version 3 is supposed to be a stable release. No (big)
new features are introduced. It's about small fixes and adding just
little things. There should be a maintenance release every month, with
a minor release every 6 to 9 months. So in this case, being dull is a
good thing.

Having said that, a new ZODB implementation might be forthcoming and
Plone 3.2 (which should be released by the end of the conference) will
be completely be 'eggified' and have a unified installer.

Plone 4 is all about making it smaller, easier and faster. Although it
might be at the expense of some backward compatibility, it should be a
big step forward.
