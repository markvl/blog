---
title: Why Plone works well for large government agencies (Ken Wasetis)
slug: why-plone-works-well-for-large-government-agencies-ken-wasetis
date: 2008-10-08T18:06:00
tags: [plone, ploneconf, ploneconf2008]
---

Ken Wasetis talked about the benefits for governmental sites to use
Plone.

<!--more-->

Ken discussed that Plone being Open Source is a good thing. There are
the obvious, know advantages, of Open Source Software. But one of the
things that make Plone attractive is that there are a lot of eyeballs
actually going through the code. This in turn results in very little
hotfixes being necessary. Combined with the fine grained security
settings available in Plone, Plone is a secure solution.

The argument of commercial vendors is that although Plone is free, you
still have to pay for the integrator. Sure, but you also have to pay
for the commercial integrator... Another argument against Plone is that
there are many more, say, PHP than Plone developers/integrators, but
how many of those PHP guys have knowledge about a CMS?

There are a several studies comparing Plone to the 'competition' and
Plone does well in those comparisons. However, the community does not
just pat itself on its back, but is also critical and looking for ways
to lower the threshold of using Plone.

What makes Plone attractive for governmental sites is, amongst other
things:

- Plone complies to the accessibility guidelines. Since those
  guidelines are mandatory for governmental sites, it is a real
  benefit to have a framework that is already accessible.
- Furthermore, Plone also has a clear audit trail/versioning.
- The community around Plone is a stable one. It is very unlikely that
  a situation like
  [Mambo/Joomla!](https://en.wikipedia.org/wiki/Joomla!#History) will
  ever occur.
- And let's not forget that Plone is road tested as fully functional
  CMS. Zope/Plone is working on sites which are able to handle
  hundreds of logged in users while serving thousands of
  visitors. (Admitted, this does take a serious server, but the
  software is up to the job.)

Ken did put out a call to stop free-loading. This can be done by
actively participating in the community, spreading the word about
Plone or simply keep using the Plone `favicon.ico`. ;-) But also
e.g. providing space for a conference can help the community.

Finally, do check out the PloneGov site.
