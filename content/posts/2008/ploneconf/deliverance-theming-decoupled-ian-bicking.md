---
title: "Deliverance: Theming Decoupled (Ian Bicking)"
slug: deliverance-theming-decoupled-ian-bicking
date: 2008-10-09T21:23:00
tags: [css, html, plone, ploneconf, ploneconf2008]
---

Deliverance can be used to theme a site without having to have any
knowledge of Plone.

<!--more-->

After
[installing Deliverance](https://pythonhosted.org/Deliverance/quickstart.html),
you can use paster to create a Deliverance project. Deliverance uses
"rules" to fetch HTML from a source and place it in the theme you
create. The source site doesn't have to be a Plone site by the way. It
can also be e.g. a PHP site or even a static site. As long as there is
HTML to process.

Deliverance is taking the resources (like the CSS files) from the
source and the theme itself and combines them. This makes the Kupu
editor work for example, but it can also result in strange situations
where the styles clash. The HTTP headers of the main content, like the
last modified header, are (almost) forwarded unchanged. So caching
with Squid is still possible.

Deliverance is, however, limited to modifying the HTML it received. It
doesn't know anything about the content. Reordering is possible,
hiding certain elements is also possible. Still, you regularly have to
dive into the underlying framework (Plone) to change the end result.

See the [Deliverance documentation](https://pythonhosted.org/Deliverance/)
for more information.
