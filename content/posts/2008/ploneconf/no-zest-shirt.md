---
title: "No Zest shirt?!?"
slug: no-zest-shirt
date: 2008-10-08T19:37:00
tags: [ploneconf, ploneconf2008]
---

What? A photo without a Zest t-shirt?

<!--more-->

Before any of my colleagues
[keelhauls](https://en.wikipedia.org/wiki/Keelhauling) me for not
wearing my Zest shirt for the conference photo: sorry, but I just
wasn't able to. My suitcase got left behind in Amsterdam and at the
moment I'm waiting for it to arrive. With a bit of luck I'll have it
this evening...

{{< note type="update" >}}
United indeed delivered my suitcase this evening! Yay!
{{< /note >}}
