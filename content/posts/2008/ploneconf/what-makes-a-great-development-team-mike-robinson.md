---
title: What makes a great development team (Mike Robinson)
slug: what-makes-a-great-development-team-mike-robinson
author: Jean-Paul Ladage
date: 2008-10-09T22:52:00
tags: [plone, ploneconf, ploneconf2008]
---

Principles to help development teams.

<!--more-->

**(Contribution by Jean-Paul Ladage)**

Process tools and technology are managed by people. Only people can
make development teams jell. Successful projects are done by people
that balance tools, technology and process the right way.

The following principles help team to jell:

- Change - The world is always changing, embrace that (Agile Manifesto)
   - minimize impact
- Simplicity - start of with a minimal mindset, less for less.
- Quality - internal and external
- Commitment - collective ownership, feel part of something
- Visibility - of progress (like we do with the XM tool)
- Collaboration - involve end-users face-to-face as soon as possible in a project.
- Focus on Value - Value is what makes customers pay us. The team has to understand WHY they are building each feature.
- Process - select the simplest process that fits a project considering:
   - Size of requirements
   - Business Criticality

Light - Medium - High processes need different approaches within your method.

Most important is that the team understands and lives by these
principles.
