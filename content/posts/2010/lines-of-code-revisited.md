---
title: Lines of code revisited
slug: lines-code-revisited
date: 2010-05-31T22:20:00
tags: [blog, django, plone]
---

After [a comment](https://twitter.com/HammerToe/status/15102276578) by
Matt Hamilton on Twitter about the lines of code in my website project
I listed in
[my previous weblog entry](/2010/05/30/switch-django/), I
decided to dive in a bit deeper.

<!--more-->

Matt wrote today:

> so, same order of magnitude of LoC and resources, but #django provides order
> of magnitude (or two) less functionality than #plone?

First of all, I think I should start by explaining better **what** I
measured. Both my Plone website and the current Django site are
managed via [buildout](https://pypi.org/project/zc.buildout/). So when I wanted to
count the line numbers of the files under source control, I started by
checking out the buildout, and making sure there were no files related
to the version control system (`.git` or `.svn` directories). Then I
simply ran "``find . | xargs wc -l``" from the buildout directory.

This lead to these numbers:

{{< list-table header=true >}}
&nbsp;
Plone
Django

Files
160
87

Lines of code
4336
3390
{{< /list-table >}}

## New numbers

In hindsight this approach was a bit too simplistic. So for this second attempt
I took another approach.

In the case of the Plone site I counted the stuff needed for the buildout and my
own products to customize the way Plone or installed add-ons behave or look like
(basically a policy and theme product).

For the Django that translates in applications that
provide the blogging functionality, configuration of the Django
project, the templates and CSS and, again, buildout related files.

In both cases I've removed unneeded files (like ``HISTORY.txt``,
``LICENSE.txt``, et cetera) and stuff I didn't use anymore.

The score now:

{{< list-table header=true >}}
&nbsp;
Plone
Django

Files
98
83

Lines of code
1927
2234
{{< /list-table >}}

Wow I didn't see that coming. So first of all there apparently was was
more cruft in the custom code for my Plone site than I figured. And for
Django I need more custom code to end up with only the functionality I
actually used.

That last conclusion isn't surprising actually: I decided to build the
blogging related code myself on top of Django, instead of reusing a
ready-made application.

So my conclusion now is that Matt's comment was an understatement: I
wrote *more* custom code and ended up with less functionality...
