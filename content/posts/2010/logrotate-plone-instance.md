---
title: Logrotate Plone instance
slug: logrotate-plone-instance
date: 2010-09-08T21:02:00
tags: [devops, plone]
---

While reading
[Plone 3 Intranets](https://www.packtpub.com/product/plone-3-intranets/9781847199089)
by Víctor Fernández de Alba, I discovered the `logreopen` command.

<!--more-->

This means that this snippet in the logrotate configuration file:

```plaintext
${buildout:directory}/var/log/instance.log {
    postrotate
        /bin/kill -USR2 $(cat ${buildout:directory}/var/instance.pid)
    endscript
}
```

Can be replaced by this:

```plaintext
${buildout:directory}/var/log/instance.log {
    postrotate
        ${buildout:directory}/bin/instance logreopen
    endscript
}
```

I don't know how well-known this command is, but at least for me it
was a new one.

(I will write a review of the book soon, but I already wanted to share
this with you. For those of you that cannot wait, check out the
[sample chapter](https://www.packtpub.com/product/plone-3-intranets/9781847199089)
from the Packt Publishing website.)
