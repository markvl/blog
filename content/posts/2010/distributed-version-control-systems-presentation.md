---
title: Distributed Version Control Systems (presentation)
slug: distributed-version-control-systems-presentation
date: 2010-02-25T06:42:00
tags: [development, git, tools]
---

On 19 February I held a presentation for my colleagues about
distributed version control systems (DVCS). My main goal was to inform
them on what I think is the next logical step in source control.

<!--more-->
My presentation can be found
[on slideshare](https://www.slideshare.net/markvl/distributed-version-control-systems-3270524)
(or as a [Keynote file](/files/distributedversioncontrolsystems.key) on this site), a summary
can be read on
[Maurits' weblog](https://maurits.vanrees.org/weblog/archive/2010/02/presentations-at-zest#mark-dvcs).

A small disclaimer: my original plan was to create an implementation
agnostic introduction to DVCS for my co-workers at
[Zest](https://zestsoftware.nl/). However, while creating the
presentation I found it easier to compare DVCS to Subversion.

Also note that the last couple of slides talk about
[git](https://git-scm.com/) and
[git-svn](https://git-scm.com/docs/git-svn)
specifically. This is because my colleagues were interested in the way
I currently use Git to work on our projects. The `git-ext-svn-clone`
command I refer to in slide 39 can be found on
[github](https://github.com/markvl/git-svn-clone-externals).
