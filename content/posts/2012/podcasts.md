---
title: Podcasts I listen to
tags: [development, podcast]
date: 2012-11-12T19:33:00
---

In this article I share the podcasts listen to on my daily commute.

<!--more-->

Note that these podcasts are not Plone or Django specific; they aren't
even about Python. Most of them are web related and keep me informed
on what's new in our field. And I also got a few really nice tips and
tricks from them.

This list of podcasts is usually enough to keep me busy during (most
of) the drive to and from work. (Which totals up to about 6 hours per
week, just to give you an idea.)

Anyway, here's the list in alphabetic order:

* [Non Breaking Space Show](https://open.spotify.com/show/2UuppyCHMIlxMnAsChx0XZ):
  interviews with the web’s best and brightest.
* [ShopTalk](https://shoptalkshow.com/) is about front end web design,
  development and UX.
* [Tech45](https://tech45.eu/) is a Dutch podcast about tech news in
  general.
* [The Big Web Show](https://5by5.tv/bigwebshow) is about everything
  web that matters.
* [The Web Ahead](https://thewebahead.net/) is weekly podcast about
  changing technologies and the future of the web.
* [This Developer's Life](https://thisdeveloperslife.com/): stories
  about developers and their lives.

I have listened to a number of other podcasts over the years, but
these are the ones I am subscribed to at the moment. If there are
other interesting (web related) podcasts you can recommend, please
let me know!
