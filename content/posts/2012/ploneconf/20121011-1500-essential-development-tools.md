---
title: Essential development tools (Kim Chee Leong)
slug: essential-development-tools-kim-chee-leong
date: 2012-10-11T15:00:00
tags: [development, plone, ploneconf]
---

Kim shows us a collection of tools that can be useful during
development.

<!--more-->

When you want to get started with Plone development, you need to know
where to get help:

- [collective-docs.readthedocs.org](https://web.archive.org/web/20120420015404/http://collective-docs.readthedocs.org/en/latest/index.html)
- Search via [DuckDuckGo](https://duckduckgo.com/),
  [Google](https://www.google.com/),
  [Stack Overflow](https://stackoverflow.com/questions/tagged/plone)
  or the [Plone mailing lists](https://lists.plone.org/mailman/listinfo)
- IRC (`#plone` on Freenode)
- Plone books

To get started with a buildout, theme or product, use
[ZopeSkel](https://pypi.org/project/ZopeSkel/) or
[Templer](https://pypi.org/project/Templer/). Also check out the local commands
these provide.

To speed up buildout, you can:

- Use version 1.6+
- Use `allow-hosts` to limit where buildout is looking
- Use timeout `-t 5`.
- Use `-N` so buildout doesn't look for newer packages
- Create a shared local buildout cache
- Check out Ross Patterson's blog post about [buildout performance improvements](https://www.rpatterson.net/blog/buildout-performance-improvements/).

Use [mr.developer](https://pypi.org/project/mr.developer/) to check out the code
of eggs in your buildout so you can easily develop on them. (And to update them
all at once when you continue working on a later moment, use `bin/develop up`.)

The package [sauna.reload](https://pypi.org/project/sauna.reload/)
picks up changes in the files of your packages and restart Zope
automatically. But somehow Zope is restarted very quickly. For Plone 3
use [plone.reload](https://pypi.org/project/plone.reload/), which
requires manual action to reload Python code or ZCML.

With
[collective.recipe.omelette](https://pypi.org/project/collective.recipe.omelette/)
it is easier to look into the code of Plone and third party addons.

[plone.app.debugtoolbar](https://pypi.org/project/plone.app.debugtoolbar/)
shows debug information and it allows you to do stuff you normally
need to do from the ZMI.

[plone.app.themeing](https://pypi.org/project/plone.app.theming/)
allows you to install a zip file with the theme (works with Diazo).

If a site is unresponsive, you can use
[mr.freeze](https://pypi.org/project/mr.freeze/) to
investigate. You can use it to print a stack trace for all threads so you
can see what's going on.  Kim used to use
[Products.signalstack](https://pypi.org/project/Products.signalstack/),
but mr.freeze can do more, like drop Zope to a `pdb` debug prompt.

Use [jarn.mkrelease](https://pypi.org/project/jarn.mkrelease/) or
[zest.releaser](https://pypi.org/project/zest.releaser/) to
release a package. It beats performing all the steps individually by
hand.

Continuous integration with [Travis CI](https://travis-ci.org/) for
open source GitHub projects or [Jenkins](https://www.jenkins.io/) for
internal company projects. Travis *can* be used for closed source
projects but they don't advertise with it and it's quite expensive.

[plone.api](https://pypi.org/project/plone.api/) is still under
development but already very useful!

[products.pdbdebugmode](https://pypi.org/project/Products.PDBDebugMode/)
allows for post-mortem debugging, but it can conflict with
sauna.reload.

Finally: see
[collective.exampledevtools](https://github.com/collective/collective.exampledevtools)
for more details.

[View the slides](https://www.slideshare.net/kaceeleong/plone-conf-2012-essential-dev-tools)
or [watch the video](https://www.youtube.com/watch?v=JojegotBiF4).
