---
title: "Keynote: Going all out on the cloud (Jan Jongboom)"
slug: keynote-going-all-out-on-the-cloud-jan-jongboom
date: 2012-10-10T10:00:00
tags: [plone, ploneconf]
---

The first technical talk of the 10th Plone conference is by Jan Jongboom, who
works for Cloud9.

<!--more-->

In 1995 JavaScript appeared and was shipped with Netscape
2.0. Although the name is similar to Java, JavaScript is not like
Java, but more like
[Scheme](https://en.wikipedia.org/wiki/Scheme_(programming_language)). The
name was chosen to ride the wave of Java's popularity. Until 1999 it
wasn't too interesting. However, Microsoft needed XMLHttpRequest (the
basis of what we now call Ajax) and that sparked new development. In
2004 Google launched Gmail and used this technology to show mails in
your page without having to refresh the whole page.

The problem with JavaScript is that the implementation differs between
browsers and browser versions. The best sold JavaScript book is
[JavaScript: The Good Parts](https://www.oreilly.com/library/view/javascript-the-good/9780596517748/).

The DOM was never meant to be scripted! And dealing with the DOM in
JavaScript was a reason for Jan to dislike JavaScript. But then
[jQuery](https://jquery.com/) appeared in 2006. The big power from
jQuery comes from [Sizzle](https://github.com/jquery/sizzle) which allows nice
selectors. And this makes working with the DOM way better.

Then JavaScript really started to pick up. With
[Node.js](https://nodejs.org/) you can even use JavaScript on the
server. But the actual power comes from the underlying platform:
[LibUV](https://github.com/libuv/libuv), which abstracts away the
operating system calls.

Writing synchronous code is easy. *Asynchronous* code however... A
simple thread on a Linux system uses 2Mb. And every time someone does
a request, a thread is started. This can become a problem when there
are requests that take a long time. And if e.g. Apache is limited to a
certain amount of memory, you are limiting the number of possible
requests. With LibUV this is managed so you don't need the 2Mb per
thread. So if you have a web application that requires a lot of
concurrent connections, you are no longer limited by the memory usage
of the threads.

Node.js is basically a JavaScript binding to LibUV, with standard
libraries in JavaScript, using the Google V8 engine.

Why use Node.js over e.g. [Twisted](https://twistedmatrix.com/trac/)?
There was no module system in JavaScript, so one could be
invented. And JavaScript is already written asynchronous, in contrast
to most Python code. This made it easier to get started.

## Cloud9

Cloud9 wanted to bring the cool stuff from other languages to
JavaScript. It's an on-line, open source, IDE. It used
[ACE](https://ace.c9.io/) for their editor. It's completely written
in HTML, CSS and JavaScript and has many features. Currently it's only
supported by Node.js. However, you should be able to run it with
Python in a couple of months.

With Cloud9 you get a free Linux
[OpenShift](https://www.openshift.com/) VM. Including a real
terminal. But the coolest feature is collaboration. Someone else can
come into your IDE and see the same things you see. He/she can see
your cursor and see you type. (Also: If you close a file, the files is
also closed in the IDE of the other person.) So you can easily debug
and work together on something. You also don't even have to leave your
browser to deploy your code.

Plone doesn't run on the OpenShift VM yet because of a LibXML
problem. But you can also bring your own server. So Jan used an Amazon
EC2 instance for his VM. The Plone Unified installer ran fine
there. He can just open and edit the files (e.g. the buildout
configuration files) from his browser.

Cloud9 is very proud of their code completion. It should become
available for Python as well in the next few months.

Since Cloud9 in open source, you can fork the
[GitHub repository](https://github.com/ajaxorg/cloud9) and contribute
if you want to. Or join Cloud9 since they are hiring.

[View the slides](https://www.slideshare.net/janjongboom/all-out-on-the-cloud-ploneconf-2012)
or [watch the video](https://www.youtube.com/watch?v=TXc4MB4LVus).

{{< note type="update" date="2021-07-26" >}}
According to
[Wikipedia](https://en.wikipedia.org/wiki/Cloud9_IDE) Cloud9 was acquired by
Amazon in 2016. Current information about Cloud9 can be found [its
product page](https://aws.amazon.com/cloud9/).
{{< /note >}}
