---
title: Are you in a hole and still digging? Or how to become an agile company in a year (true story) (Andrew Mleczko)
slug: how-to-become-agile-company-andrew-mleczko
date: 2012-10-11T11:00:00
tags: [plone, ploneconf]
---

Insight in how RedTurtle manages their projects using their own tool:
Penelope.

<!--more-->

Previously [RedTurtle](https://www.redturtle.it/) had several separate
tools: Trac, Poi, Project management, ... All those tools were
isolated and did not share accounts or information. They (RedTurtle)
wanted to become agile.

To become an agile company you need to change people and that takes
time. At RedTurtle they invited others to train them (with workshops,
discussions, etc). This way they learnt what agile is about and how to
apply it in real situations. They are using a mix of Scrum and Kanban
and adapted it to their internal use (e.g. "stories" became "customer
requests").

RedTurtle wanted to get ISO 9001:2008 certified. The certification was
the last push the team needed to become agile. The certification was
the goal and becoming agile was the way to get there. (Note that it is
a process that does not just end with getting the certificate. It is
an ongoing thing.)

RedTurtle created Penelope: agile project management software. It is
tailored for a Plone company with a team of 4--50 people. The main
goal: integration. They did not reimplement a bug tracker,
etc. Penelope makes the tools communicate with each other. It is
built in Pyramid and uses Twitter Bootstrap to unify the interface of
everything they pull in, via e.g. Google Apps and Trac.

Components of Penelope:

- Project: customer, users, groups.
- Time management: customer request, time entry, ticket.
- Documentation: Google docs, SCM (Subversion, GitHub), Gmail,
  Dropbox. (The latter two are not yet implemented.)

The documentation part is really pluggable. You don't have to use
Google Docs or Dropbox. You can even use Plone if you really want to.

Time management was the main problem before they had Penelope. With
Penelope it is easy to see the states of the work that needs to be
done and the time spent (and left) for a project. But it also has a
planning overview to see if people are overbooked or working on too
many projects at the same time. That spreadsheet is calculated
automatically. Due to the integration with the Google API, and thus also
calendars, national holidays and days off are automatically included
in the spreadsheet.

Penelope has a very fast way to enter the time they worked. The UI
makes it easy to relate the time to the right customer and
task. Besides this fast entry mode which allows you to book hours
spent on several tasks, it is also still possible to enter worked
hours on a ticket directly.

There are several reports to review the time that was worked. You can
e.g. see on a ticket level who worked on it and how long. The customer
report has a higher level overview with how many time was spent on a
certain ticket. The invoice is generated by an external application,
but Penelope can generate a report that can be attached as a justification of the invoice.

Penelope will be available via
[GitHub](https://getpenelope.github.io/) (so you can use, customise
and improve it yourself) somewhere in the near future.

[View the slides](https://www.slideshare.net/amleczko/are-you-in-a-hole-and-still-digging-or-how-to-become-an-agile-company-in-a-year-true-story)
or [watch the video](https://www.youtube.com/watch?v=USkQOWSOn2w).
