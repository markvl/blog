---
title: Jump start your development with ZopeSkel (Cris Ewing)
slug: jump-start-your-development-with-zopeskel-cris-ewing
date: 2012-10-12T14:00:00
tags: [plone, ploneconf]
---

Tutorial on how to write templates for ZopeSkel.

<!--more-->

This is the second half of Cris's
[previous talk](/2012/10/12/zopeskel-past-present-and-future-cris-ewing/). This
part is about using and extending [ZopeSkel](https://pypi.org/project/ZopeSkel/).
By creating your own templates you can start with a customer project really
fast.

## Why and when

Do you have code you write over and over again? Is the code almost the
same each time and are the places where you change stuff always the
same?  You can avoid writing all that code by creating a template.

You can write the template the way *you* want. This is just a
system. This means you can version control your own process by
documenting it in a template.

## Basics of template writing

The first question you need to ask yourself is "what do I want?" The
next thing you will want to investigate is what you have already got.
You can use "`bin/templer --list`" and "`bin/templer <template_name>
--list-variables`" to find out more about the templates ZopeSkel has
to offer. Then have a look at the code provided by the template.

To wire everything up, you need to do four basic things:

1. Create a class.
2. Import the class to a package namespace.
3. Provide an entry point.
4. Write the actual template itself.

## Tips

*Cris has already written the documentation for this tutorial. It
 currently lives on his laptop, but it should be posted online soon. I
 (Mark) decided to just write down some notes instead of trying to
 replicate Cris' documentation.*

The related documentation can be found here:

- [Templer System Manual](https://templer-manual.readthedocs.io/en/latest/index.html)
- [Templer quick start](https://templer-manual.readthedocs.io/en/latest/quickstart.html)
- [ZopeSkel documentation](https://templer-manual.readthedocs.io/en/latest/applications/zopeskel.html)

Tips:

- Do not install ZopeSkel in your global Python! Always use a
  [virtualenv](https://pypi.org/project/virtualenv/) or
  [buildout](https://pypi.org/project/zc.buildout/).
- If you want to use [pip](https://pypi.org/project/pip/), use version 1.1 or later.
- To use the package while developing, use "`python setup.py
  develop`" (so "`develop`" instead of "`install`").
- If you are using a buildout, you can also use
  [mr.developer](https://pypi.org/project/mr.developer/).
- The modes are incrementally inclusive: the questions for the
  "easy" mode are also asked in the "expert" mode, etc.
- Which namespace to choose: if you want to contribute the
  templates and they might be useful for others: use the `templer`
  namespace; for your own project/infrastructure specific
  templates, use your company's namespace.

[View the slides](https://www.slideshare.net/cewing/jumpstart-your-development-with-zopeskel-14719067).
