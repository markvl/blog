---
title: Sprinting for newcomers
slug: sprinting-for-newcomers
date: 2012-10-12T15:00:00
tags: [plone, ploneconf]
---

What is sprinting all about about?

<!--more-->

A sprint is a great way to discuss a problem with a package. If the
maintainer is not at the sprint him-/herself, then usually someone
involved in the package is available.

The most important rules:

- Don't be afraid to approach people.
- Ask questions!

Basically a sprint is a lot of people who can do anything they
want. That could be a personal project, a PLIP, a package,
etc. Everything is fine. You can also join a sprint topic of someone
else. Just do what you want to do or find interesting. Either way: ask
questions.

People always seem very busy. But people prefer helping you fixing
stuff than fixing things themselves.

There's a theory: whenever there are more than ten Plone people in a
room, they start moving the tables. `:)`.

Sprinting doesn't stop with coding. It's the other way around: the
non-coders might even be more productive. A sprint is also a great
opportunity to discuss problems or approaches.
