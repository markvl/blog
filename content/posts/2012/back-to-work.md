---
title: Back to work
slug: back-work
date: 2012-01-04T14:57:00
tags: [development, plone, tools]
---

Thanks to a tweet by [@pypi](https://twitter.com/pypi) I discovered
[collective.backtowork](https://pypi.org/project/collective.backtowork/),
created by [Patrick Gerken](https://github.com/do3cc).

<!--more-->

The package, of which version 0.5.2 was released today, is small but can be very
useful. It shows a notification when a Zope instance has started.

![Back to work example](/images/back-work.png "Back to work example")

It currently only works on Linux systems. If you know how to do
something similar for OS X or Windows, please fork
[the code](https://github.com/collective/collective.backtowork) and
add the necessary incantations.

Oh, my Zope instance is ready, so I'm back to work...

{{< note type="update" date="2021-07-21" >}}
As of version 0.6 (June 2012) Mac is supported.
{{< /note >}}
