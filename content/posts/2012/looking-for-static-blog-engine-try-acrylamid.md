---
title: Looking for a static blog engine? Try Acrylamid!
slug: looking-for-static-blog-engine-try-acrylamid
date: 2012-10-01T08:25:00
tags: [acrylamid, blog, django, plone, python]
---

Several Pythonistas switched to a static blog this year. If you are
also looking into static blog engines, give
[Acrylamid](https://posativ.org/acrylamid/) a go.

<!--more-->

Examples of persons that went static are
e.g. [Alex Clark](https://web.archive.org/web/20120924023453/http://blog.aclark.net/yes-this-blog-is-now-powered-by-pelican.html),
[Daniel Greenfeld](https://web.archive.org/web/20120913015444/http://pydanny.com/my-new-blog.html) and
[Tarek Ziadé](https://ziade.org/2012/03/05/moving-to-pelican/). What
these guys have in common is that they all use
[Pelican](https://blog.getpelican.com/).

You could follow their example and use Pelican---and it's probably a
good choice---but I recommend you also at least have a look at
Acrylamid. It is written in Python, quite easy to get up and running,
it offers all a blog needs (articles, tags, lists of articles, pages
and feeds) and the author quickly responds to issues, pull
requests and questions. You can write your content in (amongst others)
[reStructuredText](https://docutils.sourceforge.io/rst.html) and
[Markdown](https://daringfireball.net/projects/markdown/). The
templates can be [Jinja2](https://jinja.palletsprojects.com/) or
[Mako](https://www.makotemplates.org/).

Acrylamid is already great. But it is also under active development so
it will even get better!

Disclaimer: Since today I use Acrylamid for this blog (more about that
in the next article:
[migrating to Acrylamid](/2012/10/01/migrating-to-acrylamid/)). I
also contributed some code to the project. So I might be a bit
biased...
