---
title: Podcasts I listen to — 2019 edition
tags: [aws, devops, observability, podcast, python, security]
date: 2019-10-13T17:08:00
---

Most of the time I use my commute to listen to podcasts. Because they reflect my
interests at this point in time, I thought it would be nice to share my current
list.

<!--more-->

The last time I posted my selection of podcasts was back in
[2012](/2012/11/12/podcasts-i-listen-to/). Since then, a lot has changed:

* Only two out of the six podcasts on the 2012 list are still in my feed. But
  there are quite a few new podcasts to compensate.
* My commute has reduced from six hours/week to about four.
* Out of necessity I no longer listen to all episodes of all podcasts---I just
  do not have enough time to listen to them. But all podcasts in the list below
  have interesting episodes, so unsubscribing from them is also not an option.
  (I know, first world problems...)

My podcast feed with the reason why I listen to each show:

* [AWS Morning brief](https://www.lastweekinaws.com/): I use this podcast to
  quickly get up to speed on AWS related developments. Oh, and I like the snarky
  way Corey Quinn summarises the news.
* [Command line heroes](https://www.redhat.com/en/command-line-heroes): a fun
  and educational podcast about topics that interest me as a developer. For
  example season 2 was focussed open source development and season 3 was all
  about (the history of) programming languages.
* [Darknet Diaries](https://darknetdiaries.com/): interesting stories about
  {{< q >}}the dark side of the internet.{{< /q >}} I love the stories and the way
  Jack Rhysider tells them.
* [Freakonomics Radio](https://freakonomics.com/): a completely different show
  because it is not technology focussed. Stephen Dubner discusses topic I often
  don't know much about or have not thought about much. I think the episodes are
  not only educational but also fun to listen to.
* [O11ycast](https://www.heavybit.com/library/podcasts/o11ycast/): this one I
  recently discovered. Since I'm interested in the subject of observability,
  this podcast gives me new ideas to think about.
* [Real World DevOps](https://www.realworlddevops.com/): I really like Mike
  Julian's interviews with people from the tech industry about DevOps related
  topics. Unfortunately there has not been a new episode for a few months; I
  hope Mike will start podcasting again in the future.
* [Reply All](https://gimletmedia.com/shows/reply-all): a brilliant show. I had
  lost track of the hosts, PJ Vogt and Alex Goldman, after they stopped with the
  TLDR podcast. Unfortunately I did not search for them, otherwise I would have
  found Reply All a lot sooner! I listen to this podcast primarily to get
  amused; learning something from it is a nice side effect though.
* [Screaming in the cloud](https://www.lastweekinaws.com/podcast/screaming-in-the-cloud/): another podcast
  from Corey Quinn which I listen to, to learn more about cloud related topics
  from the interviews on this show.
* [Security Now](https://twit.tv/shows/security-now):  I listen to this podcast
  for the security related  news and an occasional deep dive into a technical
  topic.
* [Shop Talk Show](https://shoptalkshow.com/): although these days I'm more into
  infrastructure and automation, I like listening to this show allow to keep in
  touch with web development.
* [Talk Python to me](https://talkpython.fm/): more or less the same as with
  Shop Talk Show: I like this podcast for its (Python) development related topics.
* [Tech45](https://www.tech45.eu/): a podcast I've listened to for quite some years
  now and one of the few shows I want to listen to every single episode of. I
  enjoy the way the panel discusses the (tech related) news each week.
  Informative and "gezellig."
* [The privacy, security & OSINT show](https://soundcloud.com/user-98066669):
  the most recent addition to my list. I have only listened to a few shows yet,
  but the topics seem interesting. We'll see if this one sticks or not.

And there you have it: my list of podcasts of October 2019. (Also available in
[OPML format](/files/podcasts-2019.opml).)
