---
title: "Devopsdays Ghent 2019: random notes"
date: 2019-11-02
tags: [conference, devops, opinion, tools]
---

Where my [previous](/2019/10/29/devopsdays-ghent-2019-day-one/)
[articles](/2019/10/30/devopsdays-ghent-2019-day-two/) were focussed on
the notes I took of the talks, this article is a mix of random notes and
observations I made throughout the conference.

<!--more-->

![Belgian chocolates: "dev", "heart", "ops", "days"](/images/devopsdaysghent2019_random_notes.jpg)

## The conference

First of all: this devopsdays conference---once again---inspired me. It
refreshed my desire to make the world (or at least a part of it) a better place.

A big change in the format was that each speaker only had a 15 minute time slot.
This was done to allow more people to speak. While the organization certainly
succeeded in that regard, I felt that most talks were too short, that is: I
would have loved them to last longer.

It felt like the conference was less technical. Perhaps this was because of the
length of the talks, which meant the speakers could not go as deep into a
subject as with a 30-minute talk? Perhaps it was because there were no workshops
where I usually get (more than) my dose of technical deep dives? Perhaps it was
just me?

Note that this is **not** a complaint about the speakers. Each and every one of
them did a great job and had an inspiring talk. I really appreciate them taking
the stage and sharing their knowledge, experience and opinions with us. Thank
you all!

## Sponsors

I talked to a number of sponsors and would like to do more research on certain
products. Examples:

- [Google Cloud](https://cloud.google.com): so far my focus has been on AWS and
  Azure, but Google's cloud offering might also be interesting.
- [Logz.io](https://logz.io/): instead of managing our own Elastic stack and
  metrics collection + visualization, we might be able to use logz.io for things
  we want to run in the cloud. Apparently you can also specify that you want
  your data to stay within the EU, which is a plus.
- [Pulumi](https://www.pulumi.com/): So far I have used Ansible, Terraform and
  CloudFormation for my infrastructure-as-code needs. Pulumi allows you to code
  in e.g. Python instead of YAML or a domain specific language.
- [Rancher](https://rancher.com/): although the projects I'm involved in do not
  use Kubernetes at the moment, they might in the future. And then it might be
  useful to see what Rancher can do for us.
- [Sonaytpe](https://www.sonatype.com/): detect components with a known
  vulnerability in your stack.

## Other conferences

During the ignites on the second day, a few other conferences were
mentioned:

- [Delivery Conf](https://www.deliveryconf.com/): 21 & 22 January 2020, Seattle, WA, USA
- [FOSDEM](https://archive.fosdem.org/2020/): 1 & 2 February 2020, Bruxelles, Belgium
- [Configuration Management Camp](https://cfgmgmtcamp.eu/ghent2020/): 3--5 February 2020, Ghent, Belgium

## Home lab

One of the open spaces I joined was about home labs. I only have a small setup
at home, but I did get a few ideas out of this session that may be worth
checking out.

- [Nextcloud](https://nextcloud.com/)
- Have my Synology NAS backup stuff to AWS Glacier
- [Plex](https://www.plex.tv/); perhaps I can use this to make my DVD collection
  (which is now gathering dust) accessible again?

But the most important question, in my opinion, that was asked during this
session: {{< q >}}do you have a disaster recovery plan for your home lab?{{< /q >}}

What if I'm not around and something breaks down, like the WiFi, how does my
family get things up and running again? Spoiler: they probably won't. And this
is not their fault. I made things more complex than needed (from their
perspective that is) and failed to provide instructions on how to solve issues.

## Miscellaneous

On meetings:

- [Lean Coffee](https://agilecoffee.com/leancoffee/): an interesting, lightweight meeting format
- [POWER start](https://medium.com/@agileGeorge/power-start-your-meetings-fcdeab634bf3):
  a meeting facilitation technique to increase the effectiveness of meetings

On toil:

- Read up on the subject of toil: Google's SRE book, chapter 5:
  [Eliminating toil](https://sre.google/sre-book/eliminating-toil/)
- Learn to better recognize toil and record it (what is it, what does it cost
  in time, energy, etc and what is the frequency)
- Make time to eliminate toil
- Read up on, and think about, [immutable infrastructure](https://www.hashicorp.com/resources/what-is-mutable-vs-immutable-infrastructure)
  and see if we can use it to remove toil

Random, unrelated stuff:

- Look at [CloudFormation Lint](https://pypi.org/project/cfn-lint/)
- Read the [GitOps](https://www.weave.works/technologies/gitops/) article by Weaveworks

Something that was said during an open space about testing infrastructure-as-code:

> You deploy infrastructure not to have that infrastructure, but to run an application on top of.

I don't recall who mentioned it, when and what the context was, but
[r/devops](https://www.reddit.com/r/devops) might be fun to read.
