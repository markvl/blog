---
title: Custom Kali Linux ISO — part 1
date: 2024-02-11
tags: [kali, tools, vagrant]
---

When I wanted to create a custom Kali Linux ISO using Vagrant, the allocated
disk was not big enough. Solving this required some searching and putting
several bits of information together. This post shows how I increased the disk
size.

<!--more-->

The Kali documentation contains a nice page about [creating a custom Kali
ISO](https://www.kali.org/docs/development/live-build-a-custom-kali-iso/). That
page states that {{< q >}}[i]deally, you should build your custom
Kali ISO from within a pre-existing Kali environment, [...]{{< /q >}} so I
decided to use [Vagrant](https://www.vagrantup.com/) to create a virtual machine
which I could use.

The basic configuration for that box:

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "kalilinux/rolling"

  config.vm.provider "virtualbox" do |vb|
    # Do not display the VirtualBox GUI when booting the machine
    vb.gui = false
  end
end
```

When I started building the ISO, the default 40G disk filled up and the process
could not be completed.

To solve this, the first step was to have Vagrant resize the disk for the
machine. You can do this by adding a single line to your configuration.
(For details see the relevant
[Vagrant documentation](https://developer.hashicorp.com/vagrant/docs/disks/usage).)

{{< highlight ruby "hl_lines=3" >}}
Vagrant.configure("2") do |config|
  config.vm.box = "kalilinux/rolling"
  config.vm.disk :disk, size: "50GB", primary: true

  config.vm.provider "virtualbox" do |vb|
    # Do not display the VirtualBox GUI when booting the machine
    vb.gui = false
  end
end
{{< /highlight >}}

While this increases the size of the disk, I still had to increase the
partition in the OS. This was a bit more involved, but by adding a couple of
commands in a `config.vm.provision` section, I got it working without needing to
reboot the machine afterwards:

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "kalilinux/rolling"
  config.vm.disk :disk, size: "50GB", primary: true

  config.vm.provider "virtualbox" do |vb|
    # Do not display the VirtualBox GUI when booting the machine
    vb.gui = false
  end
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get install -y cloud-guest-utils
    swapoff -a
    echo '+10G,' | sfdisk --move-data --force /dev/sda -N 2
    partprobe
    growpart /dev/sda 1
    resize2fs /dev/sda1
    swapon -a
  SHELL
end
```

Running "`vagrant up`" now does all the heavy lifting for you. Do note that
resizing the disk and making the extra space available to the OS takes
significantly more time when creating the machine (compared to using a machine
with the default disk size). Then again: if you know this, you can plan to have
a cup of coffee (or tea if you are like me) in that time.

The end result:

{{< highlight text "hl_lines=6" >}}
┌──(vagrant㉿kali)-[~]
└─$ df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            942M     0  942M   0% /dev
tmpfs           197M  896K  196M   1% /run
/dev/sda1        48G   14G   32G  31% /
tmpfs           983M     0  983M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
tmpfs           197M  116K  197M   1% /run/user/126
vagrant         232G  173G   59G  75% /vagrant
tmpfs           197M  112K  197M   1% /run/user/1000
{{< /highlight >}}

Time to create a custom Kali ISO... See
[part 2](/2024/02/13/custom-kali-linux-iso-part-2/) for how I added extra
packages to the ISO.
