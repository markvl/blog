---
title: Custom Kali Linux ISO — part 2
date: 2024-02-13
tags: [kali, tools, vagrant]
---

In [part 1](/2024/02/11/custom-kali-linux-iso-part-1/) of this (mini) series I
described what I did to be able to build an ISO image using Vagrant. Now it's
time to actually customize it.

<!--more-->

I'll show what I have done and will provide links to the official documentation
for more in-depth information. This post is mainly for my own reference in the
future, but others might benefit from it as wel.

The basis is an existing Kali Linux environment which is setup with the build
script. See the
[Getting Ready](https://www.kali.org/docs/development/live-build-a-custom-kali-iso/#getting-ready---setting-up-the-build-script-kali-system)
section of the documentation. Long story short:

```bash
sudo apt install -y git live-build simple-cdd cdebootstrap curl
git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git
cd live-build-config
```

## Additional packages

You can include extra packages on your custom Live ISO. If these are available in the
Kali repos, it is quite simple. As described in the
[documentation](https://www.kali.org/docs/development/live-build-a-custom-kali-iso/#controlling-the-packages-included-in-your-build)
you can edit the files (in case of the default Live ISO) in
`kali-config/variant-default/package-lists/kali.list.chroot`. I decided to put
the additional packages in a new file:

```bash
echo "# Custom packages
chromium
torbrowser-launcher
" > kali-config/variant-default/package-lists/custom.list.chroot
```

## Additional APT repositories

A bit harder was the case where I wanted to add Visual Studio Code. This package
requires a separate APT repository. So let's start there first.

To be able to add the files needed, you need to create an additional directory:

```bash
mkdir -p kali-config/common/archives
```

Now you can configure the repository:

```bash
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > kali-config/common/archives/vscode.list.chroot
cp kali-config/common/archives/vscode.list.chroot kali-config/common/archives/vscode.list.binary
```

The first command creates a file with the `.chroot` extension and is used during
the chroot stage. To also have this file on the live system (to be able to use
APT later on to update the packages), I copy the `.chroot` file to one ending in
`.binary`. For more information on this subject, see the Debian documentation pages
[Customization overview](https://live-team.pages.debian.net/live-manual/html/live-manual/customization-overview.en.html)
and
[Customizing package installation](https://live-team.pages.debian.net/live-manual/html/live-manual/customizing-package-installation.en.html).

The APT repository signing key also needs to be stored in the same directory:

```bash
curl -s -o kali-config/common/archives/vscode.key https://packages.microsoft.com/keys/microsoft.asc
```

There's one more change that needs to be made. To make sure the build process
can actually use the packages from the added repository, you'll need to include a
few more packages at boot time.

To do this, edit the file `auto/config` and add an `--include` option to the
`debootstrap-options` line. Concretely this means changing this line:

```text
--debootstrap-options "--keyring=/usr/share/keyrings/kali-archive-keyring.gpg" \
```

into this:

```text
--debootstrap-options "--include=apt-transport-https,ca-certificates,openssl --keyring=/usr/share/keyrings/kali-archive-keyring.gpg" \
```

After that you are good to go. Just do not forget to add the package you want to
install (in this case `code`) to the list of packages. In my case this meant
updating `kali-config/variant-default/package-lists/custom.list.chroot`.
