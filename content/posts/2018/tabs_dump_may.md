---
title: Open tabs
date: 2018-05-12
tags: [devops, git, python, security, tabs, terraform]
---

Currently I have about 30 tabs open in the browser on my phone. Quite
a bunch of them I have open because I want to read the article in the
future, already have read the article and want to reread or act on it,
or a combination of the above. In this article I list the open tabs
(and some notes) so I can close them on my phone, but still have a
reference to them.

<!--more-->

## Development
[Better Git configuration](https://blog.scottnonnenberg.com/better-git-configuration/)
: Some tips from Scott Nonnenberg to improve your Git configuration.

[My Python Development Environment, 2018 Edition](https://jacobian.org/2018/feb/21/python-environment-2018/)
: A good description by Jacob Kaplan-Moss of how he uses
[pyenv](https://github.com/pyenv/pyenv),
[pipenv](https://pipenv.pypa.io/en/latest/) and
[pipsi](https://github.com/mitsuhiko/pipsi) for Python development.

## Operations
[BorgBackup documentation](https://borgbackup.readthedocs.io/en/stable/)
: Something I want to play around with---and perhaps use---to make
backups.

[Ops School Curriculum](https://www.opsschool.org/)
: A very comprehensive resource to learn to be an operations engineer.

[Serverless Ops: What do we do when the server goes away?](https://www.serverlessops.io/blog/serverless-ops-what-do-we-do-when-the-server-goes-away)
: Tom McLaughlin writes about the changing role of DevOps/Operations
engineers in a 'serverless' world.

[Ask HN: How do you back up your site hosted on a VPS such as Digital Ocean?](https://news.ycombinator.com/item?id=12672797)
: A bunch of comments with suggestions on how to arrange backups for a
VPS. (I&nbsp;need some inspiration for my own VPS.)

[Securely Using Amazon S3 Buckets For Server Backups](https://steemit.com/technology/@taoteh1221/securely-using-amazon-s3-buckets-for-server-backups)
: See above; this is one of the candidates.

[Awesome Sysadmin](https://github.com/kahun/awesome-sysadmin/blob/master/README.md)
: {{< q >}}A curated list of amazingly awesome open source sysadmin resources.{{< /q >}}

## Security
[Automatic Server Hardening](https://dev-sec.io/)
: Server hardening tips plus Chef, Puppet and Ansible modules. (Source:
[Cron weekly, issue 94](https://ma.ttias.be/cronweekly/issue-94/))

[Decent Security](https://decentsecurity.com/)
: Information on how to secure your devices (Windows, routers).

## DevOps
[DevOps README.md](https://github.com/chris-short/DevOps-README.md)
: {{< q >}}A curated list of things to read to level up your DevOps skills and
knowledge{{< /q >}} by Chris Short. (Source: [DevOps'ish, issue 043](https://devopsish.com/043/))

[Monitoring and Observability](https://copyconstruct.medium.com/monitoring-and-observability-8417d1952e1c)
: A great post by Cindy Sridharan explaining the difference between
monitoring and observability.

[A Model for Scaling Terraform Workflows in a Large, Complex Organization](https://www.contino.io/insights/a-model-for-scaling-terraform-workflows-in-a-large-complex-organization)
: An article by Ryan Lockard and Hibri Marzook about scaling your Terraform working practices.

[Site Reliability Guide for mybinder.org](https://mybinder-sre.readthedocs.io/en/latest/)
: This might contain useful information about how mybinder.org sets
things up and how to write this kind of documentation.

[Terraform, VPC, and why you want a tfstate file per env](https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/)
: Another Terraform article, this time by Charity Majors.

[Testing in Production, the safe way](https://copyconstruct.medium.com/testing-in-production-the-safe-way-18ca102d0ef1)
: Lots of information in this article by Cindy Sridharan.

[Working with Terraform: 10 Months In](https://medium.com/statics-and-dynamics/working-with-terraform-10-months-in-c15ade10c9b9)
: Perhaps this article by J.D. Hollis might save me some headache (if I get around to read it in time :) ).

## Miscellaneous

[Dark Matter](https://www.goodreads.com/book/show/27833670-dark-matter)
: A book recommendation that I still need to check out. This was the
first link that popped up when I Googled the title.

[Raspberry Pi Data Logger with InfluxDB and Grafana](https://engineer.john-whittington.co.uk/2016/11/raspberry-pi-data-logger-influxdb-grafana/)
: An article by John Whittington as input for my (almost dead) side
project to collect and graph data from my smart meter.
