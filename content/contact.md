---
title: Contact
permalink: /contact
type: page
---

If you want to get in touch with me, you can use (in random order):

* {{< rellink rel="me" href="https://mastodon.social/@markvl" title="Mastodon" >}}
* {{< rellink rel="me" href="https://nl.linkedin.com/in/markvanlent" title="LinkedIn" >}}
* {{< rellink rel="me" href="https://keybase.io/markvl" title="Keybase" >}}
* [Email](mailto:mark@vlent.nl)

### PGP

If you want to send me an email and want to encrypt it, you can use my
public [PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) key to
do so. You can:

   * Have PGP/GnuPG import it from a key server, e.g “`gpg --recv-keys
     0x2144528D88110619`”.
   * Download {{< rellink rel="pgpkey" href="/0x2144528D88110619.key" title="the key from this site" >}} and import it,
     e.g. “`wget -qO- https://markvanlent.dev/0x2144528D88110619.key | gpg --import`”.

Either way, make sure that the downloaded key has this fingerprint:

    1FFA 3F2D 26D6 B7FB AED7  FE5F 2144 528D 8811 0619

(To see the fingerprint, use e.g. “`gpg --fingerprint
0x2144528D88110619`”.)
