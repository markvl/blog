---
title: About
permalink: /about/
type: page
layout: page
---

This is the personal weblog of Mark van Lent.

After 10+ years of working as a developer building websites and web application
backends, I am now applying a "don't repeat yourself" mentality to (cloud)
infrastructure. For lack of a better term, I picked the title "infrastructure
developer" since that covers what I do best.

On this weblog I almost exclusively write about work related things. As a
result most older posts (until circa 2017) are related to developing web applications
and the more recent posts are more about DevOps, cloud and infrastructure related
topics.

If you like, you can find out more [about me](/about/me) or [about this website](/about/website).
