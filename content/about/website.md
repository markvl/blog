---
title: About this website
type: page
---

This site, "my corner of the internet," has been around for quite a while in one
form or another. On this page I'll take you on a trip down memory lane.

## The beginning

This website started in the 90s as a static HTML site that listed my bookmarks,
both for personal use and to easily share them with others. It lived on a server
maintained by one of my university friends on a subdomain under the `tudelft.nl`
domain.

When I got the `vlent.nl` domain in 2003 it had already been upgraded to a PHP
site and looked something like this:

![2003–2007, PHP version](/images/this_site/2003-2007-php.png "2003–2007, PHP")

## Plone

After working with [Plone](https://plone.org/) for more than a year, I used that
CMS to create a new version in [2008](/2008/08/24/new-start../). The event that
triggered me to resurrect my site was the fact that I was going to the Plone
Conference of 2008 and I wanted a blog to create
[summaries of the talks I attended](/tags/ploneconf2008). This was the result:

![2008–2009, Plone](/images/this_site/2008-2009-plone.png "2008–2009, Plone")

In 2009 I created a new theme for the site, basically a modified
version of the
[Keep it simple theme](https://pypi.org/project/plonetheme.keepitsimple/):

![2009–2010, Plone](/images/this_site/2009-2010-plone.png "2009–2010, Plone")

## Django

Two years later I changed my mind again and figured that my blog does not need a
complete content management system with extensive user and permission
management. I decided to go with a more lightweight,
[Django](https://www.djangoproject.com/) based website. This new site went live
in [2010](/2010/05/30/switch-django/). With regard to the appearance I went back
to a blue look:

![2010, Django](/images/this_site/2010-2010-django.png "2010, Django")

That version didn't last very long though. About two months later I wanted
something with more colour and I created a new theme based on the Logistix theme
from Free CSS Templates:

![2010–2012, Django](/images/this_site/2010-2012-django.png "2010–2012, Django")

## Acrylamid

I started working on a new look in the second half of 2011 but that turned out
to become a completely new site. I wanted to go "back to basics" (more or less).
I realised that I did not even need a dynamic site at all for this weblog. So I
switched to using [Acrylamid](https://github.com/posativ/acrylamid/) to build
this static blog out of posts written in Markdown. When it went live on
[October 1st in 2012](/2012/10/01/migrating-to-acrylamid/)---just in time for the 2012
Plone Conference---it looked like this:

![2012–2016, Acrylamid](/images/this_site/2012-acrylamid.png "2012–2016, Acrylamid")

After changing the fonts and some tweaking, this is what it looked
like as of mid 2016:

![2016–2021, Acrylamid](/images/this_site/2016-acrylamid.png "2016–2021, Acrylamid")

## Hugo

Acrylamid served me well for quite a number of years. It did its job well and I
would have still recommend it if the project was still maintained (which it
hasn't since 2016). And since the world of static site generators has grown
immensely since 2011, I shopped around and landed on [Hugo](https://gohugo.io/)
for a completely new version of this site.

The new version of this site is also using a different domain
(`markvanlent.dev`) and I changed the title back from "Practicing web
development" to "Mark van Lent's weblog" since the latter fits the current
content better. After many months of preparation, I migrated over on
[December 10, 2021](/2021/12/10/migrating-to-hugo/) with the site looking
like this:

![2021, Hugo](/images/this_site/2021-hugo.png "2021, Hugo")
