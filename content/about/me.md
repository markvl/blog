---
title: About Mark van Lent
type: page
---

Hi there! My name is Mark van Lent.

{{< figure
    src="/images/mark-van-lent.jpg"
    width="150px"
    height="150px"
    alt="Mark van Lent"
    class="float-left"
>}}

After 10+ years of working as a developer building websites and web
application backends, I am now applying a "don't repeat yourself" mentality to (cloud)
infrastructure. For lack of a better term, I picked the title "infrastructure
developer" since that covers what I do best.

## Education

When I was young, my parents had several home computers (starting with a
[ZX Spectrum](https://en.wikipedia.org/wiki/ZX_Spectrum) and later an
[MSX](https://en.wikipedia.org/wiki/MSX).) Although I liked playing games
on them, my love for computers really started when we got a personal computer:
an [i386](https://en.wikipedia.org/wiki/I386) computer with a whopping megabyte
of memory running [MS-DOS](https://en.wikipedia.org/wiki/MS-DOS). I started
programming on this machine ([QBasic](https://en.wikipedia.org/wiki/QBasic)) and
had fun [optimizing the
memory usage](https://en.wikipedia.org/wiki/DOS_memory_management) with e.g.
[QEMM](https://en.wikipedia.org/wiki/QEMM). Later on, during high school, I
taught myself how to write code in
[Pascal](https://en.wikipedia.org/wiki/Pascal_%28programming_language%29).

During my Computer Science study at the Delft University of Technology, I used a
number of programming languages like Assembler (for the
[PDP-11](https://en.wikipedia.org/wiki/PDP-11)) and
[Modula-2](https://en.wikipedia.org/wiki/Modula-2), but also the more commonly
used languages like C and Java. Part-time jobs and hobby lead me to code in
ColdFusion, Perl, PHP and [LaTeX](https://www.latex-project.org/).

While writing my master's thesis in 2004 someone gave me a book about
[Python](https://en.wikipedia.org/wiki/Python_(programming_language)) with the
words “you might also like this language.” And I did! After solving a couple of
[Python Challenge](http://www.pythonchallenge.com/) riddles and writing scripts
to automate some tasks, I decided to make writing Python code my day job.

## Developer jobs

That's how in January 2007 I ended up with [Zest Software](https://zestsoftware.nl/)
where I got introduced to the world of [Zope](https://zope.org/) and
[Plone](https://plone.org/) (both written in Python). I worked on a number of
projects for clients, mostly writing backend code. Since 2009 I also worked on
several project where we used [Django](https://www.djangoproject.com/) (another
web framework written in Python).

In February 2011 I switched to a different company
([Edition1](https://www.edition1.nl/)) but my work more or less remained the
same: building web applications for customers on a project basis. For most sites
we used Edition1's content management system called SwordFish, which was based
on Plone.

In 2013 I left the world of Plone behind me when I got a job at
[Fox-IT](https://www.fox-it.com/nl/) as a Django developer. I started building a
portal for the customers of their managed
[SOC](https://en.wikipedia.org/wiki/Security_operations_center) service. This
portal grew in functionality and became the tool used by the SOC analysts to
investigate incidents as part of the security service we offer to our customers.

## Infrastructure developer

The development team grew and the need for more infrastructure and automation
grew with it. Gradually my work became more about the infrastructure surrounding
the product we were developing, than about writing code for the product itself.
My days were filled with tasks related to creating and maintaining a testing
environment, writing [Ansible](https://en.wikipedia.org/wiki/Ansible_(software))
code to help deploy the product, et cetera.

In hindsight I think it was about 2018 when I was effectively no longer a
developer on the product, but focussed on (cloud) infrastructure automation.
Because I dislike the term "DevOps engineer," I decided to call myself an
"infrastructure developer." (Though on my CV and social media profiles I used
DevOps engineer when I was applying for a new job since---whether I like it or
not---that is a more familiar term.)

In the last period of my employment at Fox-IT my work was mainly to contribute
to projects that needed to run in the cloud (AWS). I helped teams automate the
deployment of their product/service. Most of the times I used tools like
Ansible, [Terraform](https://www.terraform.io/) and
[Packer](https://www.packer.io/) to get the job done.

## Mission Critical Engineer

As of January 2023 I joined [Schuberg Philis](https://schubergphilis.com/) as a
Mission Critical Engineer.
