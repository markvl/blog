imageoptim:
	docker run -it --rm -v $$(pwd):/blog -u $$(id -u $$(whoami)):$$(id -g $$(whoami)) $(REGISTRY)/blog-tools/imageoptim:latest jpegoptim --strip-com --strip-exif --strip-iptc --strip-xmp static/images/*.jpg
	docker run -it --rm -v $$(pwd):/blog -u $$(id -u $$(whoami)):$$(id -g $$(whoami)) $(REGISTRY)/blog-tools/imageoptim:latest optipng static/images/*.png
