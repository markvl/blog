# markvanlent.dev

This repository contains the code and content of my personal website:
[markvanlent.dev](https://markvanlent.dev). The current version of the site is
built with [Hugo](https://gohugo.io/).

## Deployment

The site is hosted on [Netlify](https://www.netlify.com/). So a deployment
consists of pushing to the `main` branch of this repo and Netlify handles the
rest.

## Development

To develop on this site, you first need to
[install Hugo](https://gohugo.io/getting-started/installing/).

Once Hugo is installed, generating the site can be done with a single command:

```bash
hugo
```

Note that Hugo not only generates the HTML, but also processes the
[SCSS](https://sass-lang.com/) and transforms it to CSS.

The output will be in the `public` directory.

If you want Hugo to serve the site directly from memory, plus live reload
functionality, use the following command:

```bash
hugo server
```

## Image optimization

```bash
docker login registry.gitlab.com
REGISTRY=registry.gitlab.com/markvl make imageoptim
```

(Requires personal access token.)
